from __future__ import annotations

from pathlib import Path

from BTrees.OOBTree import OOBTree
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow


class CredentialsStorage(OOBTree):
    def get_credentials(self, flow: InstalledAppFlow, path: Path) -> Credentials:
        """
        Get credentials for a given path with a given flow

        If no credentials are in the database, request the user to enter some

        Parameters
        ----------
        flow : InstalledAppFlow
            The flow object to request new credentials
        path : Path
            The path where the client_secrets.json is stored

        Returns
        -------
        Credentials
        """
        if not str(path) in self:
            self[str(path)] = flow.run_local_server()
        return self[str(path)]
