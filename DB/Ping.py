from datetime import datetime

from persistent import Persistent

from Utilities.Representations import GenericRepr


class PingStorage(GenericRepr, Persistent):
    def __init__(self, duration: float, time: datetime, error: bool = False, error_code: str = ''):
        """
        A storage object for an individual ping result

        Parameters
        ----------
        duration : float
            The round trip duration in milli seconds
        time : datetime
            The time when the ping has been started
        error : bool
            If an error occurred while doing the ping
        error_code : str
            The code of the error
        """
        assert error_code if error else not error_code, \
            f'Got either an error without an error_code or an error_code without an error. ' \
            f'error={error}, error_code={error_code}'
        self._duration: float = duration
        self._time: datetime = time
        self._error: bool = error
        self._error_code: str = error_code

    @property
    def duration(self) -> float:
        """
        Read only round trip duration of the ping

        Returns
        -------
        float
        """
        return self._duration

    @property
    def time(self) -> datetime:
        """
        Read only time when the ping has been executed

        Returns
        -------
        datetime
        """
        return self._time

    @property
    def error(self) -> bool:
        """
        Read only error

        Returns
        -------
        bool
        """
        return self._error

    @property
    def error_code(self) -> str:
        """
        Read only error_code

        Returns
        -------
        str
        """
        return self._error_code
