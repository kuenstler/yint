from typing import List

from BTrees.OOBTree import OOBTree
from fuzzywuzzy.fuzz import token_set_ratio

from Utilities.Representations import OOBTreeRepr
from Utilities.Typings import AddButtonArguments
from .Video import VideoStorage, DownloadStatus


class VideoListStorage(OOBTreeRepr, OOBTree):
    def __setitem__(self, key, value):
        """
        Set a new video

        Checks if the video fulfills the standards

        Parameters
        ----------
        key : str
            The key for the video in a format like yt:video:vid
        value : VideoStorage
            The actual Video

        Returns
        -------
        None
        """
        if not isinstance(key, str) or key.count(':') < 2 or key.split(':')[1] != 'video':
            raise RuntimeError(f'Invalid key in VideoListStorage. {key} needs to be a str, have at least 2 \':\' and '
                               f'the center part needs to be :video:.')
        if not key.startswith(('yt',)):
            raise RuntimeError(f'Key must be one of the following: "yt"')
        if not isinstance(value, VideoStorage):
            raise RuntimeError(f'The value for VideoListStorage needs to be VideoStorage, got {type(value)} instead.')
        if key != value.vid:
            raise RuntimeError(f'Tried to write video with vid={value.vid} to position {key}. Key and vid must not be'
                               f' different.')
        super().__setitem__(key, value)

    def search_for(self, search: str, threshold: int = 90) -> List[VideoStorage]:
        """
        Search for a given string in all videos

        Parameters
        ----------
        search : str
            The search string
        threshold : int
            The threshold whether something is a result or not

        Returns
        -------
        list of VideoStorage
            A list of matching videos
        """
        return [value for value in self.values() if token_set_ratio(value.search_string, search) >= threshold]

    @property
    def download_items(self) -> List[AddButtonArguments]:
        """
        Get all items needed to fill in GenericLists add_button to generate the needed entries

        Returns
        -------
        look at the type hints
            The list of items
        """
        return [i.get_add_button_arguments() for i in self.values()
                if i.download_status == DownloadStatus.WAITING_FOR_DOWNLOAD_PERMISSION]

    @property
    def like_items(self) -> List[AddButtonArguments]:
        """
        Get all items needed to fill in GenericLists add_button to generate the needed entries

        Returns
        -------
        look at the type hints
            The list of items
        """
        return [i.get_add_button_arguments(like=True) for i in self.values() if i.like is None]
