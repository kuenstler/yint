from __future__ import annotations

from datetime import datetime, timedelta
from typing import TYPE_CHECKING, Tuple, Optional

from persistent import Persistent

from TUI.Runtime import Runtime
from TUI.Stream import StreamList
from Utilities.Representations import GenericRepr
from Utilities.Typings import SID
from Utilities.YoutubeFeatures import NormalVideoResult

if TYPE_CHECKING:
    from .Channel import ChannelStorage


class StreamStorage(GenericRepr, Persistent):
    def __init__(self, sid: SID, start_time: datetime, title: str, channel: ChannelStorage, description: str = None,
                 rating: float = None, rating_count: int = None, views: int = 0, no_like: bool = False):
        """
        A DB object for a stream

        Parameters
        ----------
        sid : SID
            The id of the stream
        start_time : datetime
            The time when the stream started or will start
        title : str
            The title of the stream
        channel : ChannelStorage
            The channel this stream happens on
        description : str, optional
            Description of the stream, if available
        rating : float, optional
            The rating from 0 to 5 of this video, if available
        rating_count : int, optional
            The number of ratings in total, if available
        views : int, optional
            The number of peak views
        no_like : bool
            If the stream should be downloaded at all

        Notes
        -----
        As far as I know, you can't get ids for Twitch streams while they are running
        """
        self.sid: SID = sid
        self.title: str = title
        self.start_time: datetime = start_time
        self.channel: ChannelStorage = channel
        self.channel.add_part(self)
        self.ended: bool = no_like
        self.description: Optional[str] = description
        self.rating: Optional[float] = rating
        self.rating_count: Optional[int] = rating_count
        self.views: int = views
        self.channel.add_part(self)

    @property
    def base_id(self) -> str:
        """
        Get the base id without yt:stream:

        Returns
        -------
        str
        """
        return self.sid.split(':')[2]

    @property
    def button_arguments(self) -> Tuple[bool]:
        """
        Returns arguments that are needed to configure a button

        Returns
        -------
        tuple of bool
            The option
        """
        return self.channel.button_arguments

    @property
    def readable_description(self) -> str:
        """
        Returns a readable description for this video

        Returns
        -------
        str
            The description
        """
        offset: timedelta = datetime.now() - self.start_time
        return (
            f'{self.start_time.ctime()} '
            + f'since {offset}' if offset > timedelta() else f'in {offset * -1}'
                                                             + f' from {self.channel.name}.'
        )

    def register_lists(self):
        """
        Register this widget in a running StreamList, if any

        Returns
        -------
        None
        """
        if not hasattr(Runtime, 'frame'):
            print('Running in testing mode')
            return
        body = Runtime.frame.body
        if isinstance(body, StreamList):
            body.add_button((self.title,), self.start_time.isoformat(), self.readable_description,
                            self.button_arguments)

    @property
    def readable_name(self) -> str:
        """
        Get a name with, depending on the platform, useful information

        Returns
        -------
        str
        """
        if self.sid.startswith('yt'):
            return f'{self.title} on {self.start_time.ctime()}'
        if self.sid.startswith('tw'):
            return f'{self.channel.name} on {self.start_time.ctime()}'

    @staticmethod
    def get_sid_from_yt_id(yt_id: str) -> SID:
        """
        Return a SID from a given simple id

        Parameters
        ----------
        yt_id : str
            The simple id

        Returns
        -------
        SID
        """
        return SID(f'yt:stream:{yt_id}')

    @staticmethod
    def get_sid_from_tw_id(tw_id: str) -> SID:
        """
        Return a SID from a given simple id

        Parameters
        ----------
        tw_id : str
            The simple id

        Returns
        -------
        SID
        """
        return SID(f'tw:stream:{tw_id}')

    @staticmethod
    def from_youtube_result(stream_result: NormalVideoResult, channel: ChannelStorage,
                            no_like: bool = False) -> StreamStorage:
        """
        Get a StreamStorage from a given stream_result with the help of a channel

        Parameters
        ----------
        stream_result : NormalVideoResult
            The result of the search/id lookup
        channel : ChannelStorage
            The channel
        no_like : bool
            If the stream should be downloaded at all

        Returns
        -------
        StreamStorage
        """
        like: int = int(stream_result['statistics']['likeCount'])
        dislike: int = int(stream_result['statistics']['dislikeCount'])
        return StreamStorage(
            StreamStorage.get_sid_from_yt_id(stream_result["id"]),
            datetime.fromisoformat(stream_result['liveStreamingDetails']['scheduledStartTime']),
            stream_result['snippet']['title'],
            channel,
            description=stream_result['snippet']['description'],
            rating=like / (like + dislike) * 5,
            rating_count=like + dislike,
            views=int(stream_result['statistics']['viewCount']),
            no_like=no_like
        )
