from __future__ import annotations

import os
from datetime import datetime
from enum import Enum
from typing import TYPE_CHECKING, Tuple, Optional

from persistent import Persistent

from TUI.Runtime import Runtime
from Utilities.LuridDetection import lurid_detection
from Utilities.Representations import GenericRepr
from Utilities.Settings import Youtube
from Utilities.Typings import VID, AddButtonArguments
from Utilities.YoutubeFeatures import NormalVideoResult

if TYPE_CHECKING:
    from .Channel import ChannelStorage


class DownloadStatus(int, Enum):
    NOT_DOWNLOADED = 0
    ALREADY_DOWNLOADED = 1
    CURRENTLY_DOWNLOADING = 2
    WAITING_FOR_DOWNLOAD_PERMISSION = 3
    NEVER_GOING_TO_GET_DOWNLOADED = 4


class MarkWatched(int, Enum):
    NO_NOT_MARK_AS_WATCHED = 0
    MARK_AS_WATCHED = 1
    TO_ASK = 2


class VideoStorage(GenericRepr, Persistent):
    def __init__(self, vid: VID, title: str, description: str, channel: ChannelStorage, published: datetime,
                 rating: float, rating_count: int, views: int, thumbnail_url: str, no_like: bool = False):
        """
        A DB object of a Video

        Parameters
        ----------
        vid : str
            Video id
        title : str
            Title of the video
        description : str
            Description of the video
        channel : ChannelStorage
            The channel this video originates
        published : datetime
            The date this video has been published
        rating : float
            The rating from 0 to 5 of this video
        rating_count : int
            The number of ratings in total
        views : int
            The number of views
        thumbnail_url : str
            The url of the thumbnail
        no_like : bool
            If the user should be asked about downloading and liking

        Notes
        -----
        A object of this kind should be stored at a full ID whilst containing a short id
        ie. DB['yt:video:vid'] = VideoStorage('yt:video:vid', ...)
        """
        super().__init__()
        self.vid: VID = vid
        self.title: str = title
        self.description: str = description
        self.channel: ChannelStorage = channel
        self.channel.add_part(self)
        self.published: datetime = published
        self.rating: float = rating
        self.rating_count: int = rating_count
        self.views: int = views
        self.thumbnail_url: str = thumbnail_url
        self.download_status: DownloadStatus = (
            DownloadStatus.NEVER_GOING_TO_GET_DOWNLOADED if no_like
            else DownloadStatus.NOT_DOWNLOADED if channel.instant
            else DownloadStatus.WAITING_FOR_DOWNLOAD_PERMISSION
        )
        self.mark_watched: MarkWatched = MarkWatched.MARK_AS_WATCHED if channel.instant else MarkWatched.TO_ASK
        # None means getting asked, 0 means no like and any number greater than 0 means a number of likes
        # Any number less than 0 means the respective count of dislikes
        self.like: Optional[int] = 0 if no_like else None
        self.lurid_title: str = lurid_detection(self.title)
        self.channel.add_part(self)

    @property
    def base_id(self) -> str:
        """
        Get the base id without yt:video:

        Returns
        -------
        str
        """
        return self.vid.split(':')[2]

    @property
    def button_arguments(self) -> Tuple[bool, bool]:
        """
        Returns arguments that are needed to configure a button

        Returns
        -------
        tuple of 2 bool
            The options
        """
        return self.download_status != DownloadStatus.ALREADY_DOWNLOADED, self.channel.button_arguments[0]

    @property
    def like_button_arguments(self) -> Tuple[bool, ...]:
        """
        Returns arguments that are needed to configure a like button

        Returns
        -------
        tuple of 7 bool
            The options
        """
        return (*self.button_arguments, *Youtube.like_accounts())

    @property
    def readable_description(self) -> str:
        """
        Returns a readable description for this video

        Returns
        -------
        str
            The description
        """
        return (
                self.description.split('\n')[0]
                + f'; with {self.views} views and {int(self.rating * self.rating_count / 5)} likes.'
                + f' from {self.channel.name}.'
        )

    def register_lists(self):
        """
        Register this widget in a running DownloadLikePendingList, if any

        Returns
        -------
        None
        """
        if not hasattr(Runtime, 'frame'):
            print('Running in testing mode')
            return
        if self.download_status == DownloadStatus.WAITING_FOR_DOWNLOAD_PERMISSION:
            os.write(Runtime.main_pipe, f'add_dwnload\x1f{self.vid}\n'.encode())
        if self.like is None:
            os.write(Runtime.main_pipe, f'add_like\x1f{self.vid}\n'.encode())

    @property
    def search_string(self) -> str:
        """
        Returns a string which is useful for search functions

        Returns
        -------
        str
            The search str
        """
        return (
                f'{self.title} {self.vid} '
                + self.description.replace('\n', ' ')
                + f' {self.channel.name} {self.published.ctime()}'
        )

    @property
    def url(self) -> str:
        """
        Get the url of this entry

        Returns
        -------
        str
            The url
        """
        return f'https://youtube.com/watch?v={self.base_id}'

    @staticmethod
    def get_vid_from_yt_id(yt_id: str) -> VID:
        """
        Return a VID from a given simple id

        Parameters
        ----------
        yt_id : str
            The simple id

        Returns
        -------
        VID
        """
        return VID(f'yt:video:{yt_id}')

    @staticmethod
    def from_youtube_search_result(video_result: NormalVideoResult, channel: ChannelStorage,
                                   no_like: bool = False) -> VideoStorage:
        """
        Get a VideoStorage from a given video_result with the help of a channel

        Parameters
        ----------
        video_result : NormalVideoResult
            The result of the search/id lookup
        channel : ChannelStorage
            The channel
        no_like : bool
            If the user should be asked about downloading and liking

        Returns
        -------
        VideoStorage
        """
        like: int = int(video_result['statistics']['likeCount'])
        dislike: int = int(video_result['statistics']['dislikeCount'])
        return VideoStorage(
            VideoStorage.get_vid_from_yt_id(video_result["id"]),
            video_result['snippet']['title'],
            video_result['snippet']['description'],
            channel,
            datetime.fromisoformat(video_result['snippet']['publishedAt']),
            like / (like + dislike) * 5,
            like + dislike,
            int(video_result['statistics']['viewCount']),
            (video_result['snippet']['thumbnails']['maxres']['url']
             if 'maxres' in video_result['snippet']['thumbnails'].keys()
             else ''),
            no_like=no_like
        )

    def get_add_button_arguments(self, like: bool = False) -> AddButtonArguments:
        return ((self.lurid_title,), self.vid, self.readable_description,
                self.like_button_arguments if like else self.button_arguments)
