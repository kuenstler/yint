from persistent.list import PersistentList

from Utilities.Representations import ListRepr
from Utilities.Typings import VID


class DownloadListStorage(ListRepr, PersistentList):
    def add_video(self, vid: VID, position: int = None):
        """
        Add a video to this list

        Parameters
        ----------
        vid : VID
            The id of the video
        position : int, optional
            The position where to put the video in, None will use the last position

        Returns
        -------
        None
        """
        if position is None:
            position = len(self)
        self.insert(position, vid)

    def get_video(self) -> VID:
        """
        Get and remove the first video from the list

        Returns
        -------
        VID, optional
            The id of the video, if no video is present, it returns None
        """
        if self:
            return self[0]

    def remove_video(self, vid: VID):
        """
        Remove the video with vid

        Parameters
        ----------
        vid : VID
            Id of the video to remove

        Returns
        -------
        None
        """
        if vid in self:
            self.remove(vid)

    def move_up(self, vid: VID, top: bool = False):
        """
        Move the video with vid up

        Parameters
        ----------
        vid : VID
            The id of the video
        top : bool
            Whether to put the video at the top of the list

        Returns
        -------
        None

        Raises
        ------
        RuntimeError
            If the video either is not in the list, or the topmost entry
        """
        if vid not in self:
            raise RuntimeError(f'Tried to move entry {vid} up but it wa not in the list.')
        index = self.index(vid)
        if index == 0:
            raise RuntimeError(f'Tried to move {vid} up but it was the topmost entry.')
        if top:
            del self[index]
            self.insert(0, vid)
        else:
            self[index], self[index - 1] = self[index - 1], self[index]

    def move_down(self, vid: VID, bottom: bool = False):
        """
        Move the video with vid down

        Parameters
        ----------
        vid : VID
            The id of the video
        bottom : bool
            Whether to put the video at the bottom of the list

        Returns
        -------
        None

        Raises
        ------
        RuntimeError
            If the video either is not in the list, or the bottommost entry
        """
        if vid not in self:
            raise RuntimeError(f'Tried to move entry {vid} up but it wa not in the list.')
        index = self.index(vid)
        if index == len(self):
            raise RuntimeError(f'Tried to move {vid} down but it was the bottommost entry.')
        if bottom:
            del self[index + 1]
            self.append(vid)
        else:
            self[index], self[index + 1] = self[index + 1], self[index]
