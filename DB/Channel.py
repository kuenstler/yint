from __future__ import annotations

from threading import Lock
from typing import Tuple, List, Union

from persistent import Persistent

from Utilities.Representations import GenericRepr
from Utilities.Settings import Settings
from Utilities.Typings import CID
from Utilities.YoutubeFeatures import ChannelResult
from .Stream import StreamStorage
from .Video import VideoStorage

lock: Lock = Lock()


class ChannelStorage(GenericRepr, Persistent):
    def __init__(self, cid: CID, name: str, description: str, subscribed: bool, instant: bool = False):
        """
        A DB object for a channel

        Parameters
        ----------
        cid : CID
            The id of the channel
        name : str
            The name of the channel
        description : str
            The description of the channel
        subscribed : bool
            If the channel has been subscribed
        instant : bool
            Whether or not to download videos as soon as they are found
        """
        super().__init__()
        self._cid: CID = cid
        self._name: str = name
        self._description: str = description
        self._subscribed: bool = subscribed
        self._instant: bool = instant
        self._parts: List[Union[VideoStorage, StreamStorage]] = []

    @property
    def base_id(self) -> str:
        """
        Get the base id without yt:channel:

        Returns
        -------
        str
        """
        return self.cid.split(':')[2]

    @property
    def button_arguments(self) -> Tuple[bool]:
        """
        Returns arguments that are needed to configure a button

        Returns
        -------
        tuple of bool
            The option
        """
        return tuple((self._subscribed,))

    def add_part(self, part: [VideoStorage, StreamStorage]):
        """
        Add another part (video/stream) to the channel

        Parameters
        ----------
        part : VideoStorage or StreamStorage
            The part to add

        Returns
        -------
        None
        """
        self._parts.append(part)

    @property
    def name(self) -> str:
        """
        Name of the channel

        Returns
        -------
        str
        """
        return self._name

    @name.setter
    def name(self, value: str):
        assert isinstance(value, str)
        self._name = value

    @property
    def description(self) -> str:
        """
        Description of the channel

        Returns
        -------
        str
        """
        return self._description

    @description.setter
    def description(self, value: str):
        assert isinstance(value, str)
        self._description = value

    @property
    def cid(self) -> CID:
        """
        Channel id of the channel

        Returns
        -------
        CID
        """
        return self._cid

    @property
    def url(self) -> str:
        if self.cid.startswith('yt'):
            return f'https://youtube.com/channel/{self.base_id}'
        if self.cid.startswith('tw'):
            return f'https://twitch.tv/{self.base_id}'

    @property
    def search_string(self) -> str:
        """
        Returns a string which is useful for search functions

        Returns
        -------
        str
            The search str
        """
        return self.name + ' ' + self.description.replace('\n', ' ')

    @property
    def subscribed(self) -> bool:
        """
        Subscription status

        Returns
        -------
        bool
        """
        return self._subscribed

    @subscribed.setter
    def subscribed(self, value: bool):
        assert isinstance(value, bool)
        self._subscribed = value
        with lock:
            Settings.db_channel_dirty_bit = True

    @property
    def instant(self) -> bool:
        """
        Instant download status

        Returns
        -------
        bool
        """
        return self._instant

    @instant.setter
    def instant(self, value: bool):
        assert isinstance(value, bool)
        self._instant = value
        with lock:
            Settings.db_channel_dirty_bit = True

    @staticmethod
    def get_cid_from_yt_id(yt_id: str) -> CID:
        """
        Return a CID from a given simple id

        Parameters
        ----------
        yt_id : str
            The simple id

        Returns
        -------
        CID
        """
        return CID(f'yt:channel:{yt_id}')

    @staticmethod
    def get_cid_from_tw_id(tw_id: str) -> CID:
        """
        Return a CID from a given simple id

        Parameters
        ----------
        tw_id : str
            The simple id

        Returns
        -------
        CID
        """
        return CID(f'tw:channel:{tw_id}')

    @staticmethod
    def from_youtube_result(channel_result: ChannelResult) -> ChannelStorage:
        """
        Get a ChannelStorage from a given channel_result
        
        Parameters
        ----------
        channel_result : ChannelResult
            The result

        Returns
        -------
        ChannelStorage
        """""
        return ChannelStorage(
            ChannelStorage.get_cid_from_yt_id(channel_result["id"]),
            channel_result['snippet']['title'],
            channel_result['snippet']['description'],
            False
        )
