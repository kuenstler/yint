from BTrees.OOBTree import OOBTree

from Utilities.Representations import OOBTreeRepr
from .Stream import StreamStorage


class StreamListStorage(OOBTreeRepr, OOBTree):
    def __setitem__(self, key, value):
        if not isinstance(key, str) or key.count(':') < 2 or key.split(':')[1] != 'stream':
            raise RuntimeError(f'Invalid key in VideoListStorage. {key} needs to be a str, have at least 2 \':\' and '
                               f'the center part needs to be :video:.')
        if not key.startswith(('yt', 'tw')):
            raise RuntimeError(f'Key must be one of the following: "yt"')
        if not isinstance(value, StreamStorage):
            raise RuntimeError(f'The value for VideoListStorage needs to be VideoStorage, got {type(value)} instead.')
        if key != value.sid:
            raise RuntimeError(f'Tried to write video with vid={value.vid} to position {key}. Key and vid must not be'
                               f' different.')
        super().__setitem__(key, value)
