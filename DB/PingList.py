from datetime import datetime, timedelta
from typing import Tuple, List, TypedDict, Dict, Callable

from BTrees.IOBTree import IOBTree

from DB.Ping import PingStorage
from Utilities.Representations import IOBTreeRepr


class Statistics(TypedDict):
    min: float
    avg: float
    max: float
    err: int


StatisticsDict = Dict[int, Statistics]


class PingListStorage(IOBTreeRepr, IOBTree):
    def statistics(self, interval: timedelta, search_in: List[PingStorage] = None) \
            -> Tuple[float, float, float, int, List[PingStorage]]:
        """
        Get statistics for a given interval

        Parameters
        ----------
        interval : timedelta
            The interval to search in from now on
        search_in :
            A given list to search in, should be a sub list of this list

        Returns
        -------
        float
            The minimum value in this interval
        float
            The average of this interval
        float
            The maximum of this interval
        int
            The number of errors in this interval
        list of PingStorage
            All found pings in this interval
        """
        earliest = datetime.now() - interval
        if search_in:
            interesting: List[PingStorage] = [i for i in search_in if i.time > earliest]
        else:
            earliest = int(earliest.timestamp())
            interesting: List[PingStorage] = [i for i in self.values(earliest)]
        if not interesting:
            return 0, 0, 0, 0, []
        minimum: float = min(i.duration for i in interesting)
        average: float = sum(i.duration for i in interesting) / len(interesting)
        maximum: float = max(i.duration for i in interesting)
        errors: int = sum(1 for i in interesting if i.error)
        return minimum, average, maximum, errors, interesting

    def combined_statistics(self) -> StatisticsDict:
        """
        Get statistics for 60 minutes, 10 minutes and 1 minute

        Returns
        -------
        dict of 1, 10 and 60 and Statistics
            The statistics for given minutes
        """
        out: Dict[int, Statistics] = {}
        minimum, average, maximum, errors, next_list = self.statistics(timedelta(hours=1))
        out[60] = {'min': minimum, 'avg': average, 'max': maximum, 'err': errors}
        minimum, average, maximum, errors, next_list = self.statistics(timedelta(minutes=10), search_in=next_list)
        out[10] = {'min': minimum, 'avg': average, 'max': maximum, 'err': errors}
        minimum, average, maximum, errors, _ = self.statistics(timedelta(minutes=1), search_in=next_list)
        out[1] = {'min': minimum, 'avg': average, 'max': maximum, 'err': errors}
        return out

    @staticmethod
    def combine_minutes(db_dict: StatisticsDict, function: Callable, location: str, ignore: bool) -> float:
        """
        Get the statistics from several minutes combined

        Parameters
        ----------
        db_dict : StatisticsDict
            A dictionary returned from combined_statistics
        function : callable
            A function to calculate each individual score
        location : str
            The location in the dict
        ignore : bool
            Ignore the value for 1 minute

        Returns
        -------
        float
            The combined score
        """
        assert location in ('avg', 'max', 'min', 'err')
        minute60 = function(db_dict[60][location])
        minute10 = function(db_dict[10][location])
        minute1 = function(db_dict[1][location])
        if ignore:
            return minute60 * 0.3 + minute10 * 0.7
        return minute60 * 0.2 + minute10 * 0.4 + minute1 * 0.4

    # All values are more sensitive if they are lower

    @staticmethod
    def convert_average(average: float) -> float:
        """
        Convert an individual average value into an average score

        Parameters
        ----------
        average : float
            The average

        Returns
        -------
        float
            The score
        """
        score = 1 - (average / 3000) ** 0.45
        if score < 0 or average == 0:
            score = 0
        return score

    @staticmethod
    def convert_errors(errors: int) -> float:
        """
        Convert an individual error value into an error score

        Parameters
        ----------
        errors : int
            The error count

        Returns
        -------
        float
            The score
        """
        score = 1 - (errors / 100) ** 0.3
        return score

    @staticmethod
    def convert_max(maximum: float) -> float:
        """
        Convert an individual maximum value into an maximum score

        Parameters
        ----------
        maximum : float
            The maximum

        Returns
        -------
        float
            The score
        """
        score = 1 - (maximum / 8000) ** 0.4
        if score < 0 or maximum == 0:
            score = 0
        return score

    def calculate_score(self) -> float:
        """
        Calculate an internet score based on the last hour of pings

        Returns
        -------
        float
            THE SCORE
        """
        db_array = self.combined_statistics()
        ignore = False
        if db_array[1]['err'] == 100:
            return 0
        if db_array[10]['err'] == 0 and db_array[10]['avg'] == 0:
            return 1
        if db_array[1]['avg'] == 0 and db_array[1]['err'] == 0:
            ignore = True
        score_average = self.combine_minutes(db_array, self.convert_average, 'avg', ignore)
        score_errors = self.combine_minutes(db_array, self.convert_errors, 'err', ignore)
        score_max = self.combine_minutes(db_array, self.convert_max, 'max', ignore)
        if score_errors > 0.9:
            score = score_average * 0.45 + score_errors * 0.1 + score_max * 0.45
        else:
            score = score_average * 0.3 + score_errors * 0.4 + score_max * 0.3
        multi = -0.01 * db_array[1]['avg'] + 1.25
        if multi > 1 and db_array[1]['avg'] > 0:
            score *= multi
        if score > 1:
            score = 1
        return score
