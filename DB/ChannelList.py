from typing import List, Dict

from BTrees.OOBTree import OOBTree
from fuzzywuzzy.fuzz import token_set_ratio

from Utilities.Representations import OOBTreeRepr
from Utilities.Typings import AddButtonArguments
from .Channel import ChannelStorage

PLATFORMS: Dict[str, str] = {'tw': 'Twitch', 'yt': 'Youtube'}


class ChannelListStorage(OOBTreeRepr, OOBTree):
    def __setitem__(self, key, value):
        """
        Set a new channel

        Checks if the channel fulfills the standards

        Parameters
        ----------
        key : str
            The key for the video in a format like yt:channel:cid
        value : ChannelStorage
            The actual channel

        Returns
        -------
        None
        """
        if not isinstance(key, str) or ':channel:' not in key:
            raise RuntimeError(f'Got an invalid key {key} which either is not a str or does not contain :channel:')
        if not key.split(':')[0] in PLATFORMS.keys():
            raise RuntimeError(f'Key must be one of the following: "yt", "tw"')
        if not isinstance(value, ChannelStorage):
            raise RuntimeError(
                f'The value for ChannelListStorage needs to be ChannelStorage, got {type(value)} instead.')
        if key != value.cid:
            raise RuntimeError(f'Tried to write channel with cid={value.cid} to position {key}. Key and vid must not be'
                               f' different.')
        super().__setitem__(key, value)

    def search_for(self, search: str, channel_type: str = '') -> List[ChannelStorage]:
        assert channel_type in ('', 'yt', 'tw'), 'Channel_type must be one of the following: "yt", "tw"'
        return [i for i in self.values() if i.cid.startswith(channel_type) and token_set_ratio(i.search_string, search)]

    @property
    def sync_channels(self) -> List[ChannelStorage]:
        """
        Get all channels that need to get synchronized

        Returns
        -------
        list of ChannelStorage
        """
        return [i for i in self.values() if i.subscribed]

    @property
    def subscription_items(self) -> List[AddButtonArguments]:
        """
        Get all items needed to fill in GenericLists add_button to generate the needed entries

        Returns
        -------
        look at the type hints
            The list of items
        """
        return [(
            (i.name, PLATFORMS[i.cid.split(':')[0]]),
            i.cid,
            i.description,
            (i.cid.startswith('yt'),)
        ) for i in self.values() if i.subscribed]
