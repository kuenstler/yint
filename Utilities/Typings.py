from typing import NewType, Tuple, Optional

VID: NewType = NewType('Video ID', str)
CID: NewType = NewType('Channel ID', str)
SID: NewType = NewType('Stream ID', str)
Pipe: NewType = NewType('Pipe', int)
# for the usage of main_pipe, see TUI.PipeHandling
AddButtonArguments = Tuple[Tuple[str, ...], str, str, Optional[Tuple[bool, ...]]]
