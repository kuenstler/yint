import os
import subprocess
from typing import List


def traceroute(to: str, exit_on_foreign: bool = False, max_hop: int = 64, timeout: int = 3) -> List[str]:
    """
    Get a traceroute to a given target

    Parameters
    ----------
    to : str
        The destination
    exit_on_foreign : bool
        Whether to exit on the first foreign node, compares the first two blocks
    max_hop : int
        The maximal amount of hops to do
    timeout : int
        The timeout of each hop

    Returns
    -------
    list of str
        The list of ip addresses

    Raises
    ------
    EnvironmentError
        If the environment is not POSIX compatible
    ConnectionError
        If traceroute fails

    Notes
    -----
    If exit_on_foreign is True, it will only return the first foreign pi address
    It's not a pure python implementation because those require superuser privileges
    """
    if os.name != 'posix':
        raise EnvironmentError('Traceroute only works on POSIX like operating systems')
    process: subprocess.Popen = subprocess.Popen(
        ['traceroute', '-m', str(max_hop), '-w', str(timeout), to], stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    out_list: List[str] = []
    home_ip: str = ''
    for line in iter(process.stdout.readline, b''):
        string: str = line.decode()
        if not string.split()[0].isdecimal():
            continue
        ip_address: str = string.split()[1]
        if not home_ip:
            home_ip = '.'.join(ip_address.split('.')[:2]) + '.'
        if exit_on_foreign and not ip_address.startswith(home_ip):
            process.kill()
            return [ip_address]
        out_list.append(ip_address)
    if process.poll() or not out_list:
        # traceroute: unknown host
        raise ConnectionError('Traceroute did not work', process.communicate()[1][12:].decode().replace('\n', ''))
    return out_list


def ping(to: str, count: int = 1) -> List[float]:
    """
    Get a ping to a given target

    Parameters
    ----------
    to : str
        The destination
    count : int
        The number of pings to do

    Returns
    -------
    list of float
        The ping results

    Raises
    ------
    EnvironmentError
        If the environment is not POSIX compatible
    ConnectionError
        If ping fails

    Notes
    -----
    It's not a pure python implementation because those require superuser privileges
    """
    if os.name != 'posix':
        raise EnvironmentError('Ping only works on POSIX like operating systems')
    process: subprocess.Popen = subprocess.Popen(
        ['ping', '-c', str(count), to], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True
    )
    stdout, stderr = process.communicate()
    out_list: List[float] = [float(i.split()[-2].split('=')[-1]) for i in stdout.split('\n')
                             if i.lower().startswith('64 bytes')]
    if process.poll() or not out_list:
        raise ConnectionError('Ping did not work', stderr[6:].replace('\n', '') or 'Unknown error')
    return out_list
