from math import log10
from typing import List, Tuple

from .Settings import Settings


def number_interpretation(string: str) -> float:
    """
    Interpret a number from a given string regardless of the format

    Parameters
    ----------
    string : str
        The string to find the number in

    Returns
    -------
    float
        The number found

    Notes
    -----
    Numbers like 10.000 are being interpreted as int(10000) instead of float(10.0)
    likewise 10,000 will get interpreted as int(10000) instead of float(10.0)
    Numbers like 10,0,0,0 will get interpreted as int(10000), digit group separators will get ignored
    """
    # Remove unwanted characters
    potential_characters: List[str] = [i for i in string if i.isdecimal() or i in ('.', ',')]
    # Notes if the german type of decimal mark is used as in 10.000,0
    german_comma: bool = False
    # Counts to be used later on
    comma_count: int = potential_characters.count(',')
    dot_count: int = potential_characters.count('.')
    if (
            not comma_count and dot_count  # 10.000
            or comma_count and dot_count and potential_characters.index(',') > potential_characters.index('.')
            # 10.000,00
    ):
        # All the possible cases of the german type of decimal marks
        german_comma = True
    # Remove unused digit group separators
    potential_characters = [i for i in potential_characters if i.isdecimal() or i == '.' and not german_comma
                            or i == ',' and german_comma]
    # Get the position of the decimal mark
    separator_position: int = (
        potential_characters.index(',') if german_comma and comma_count
        else potential_characters.index('.') if not german_comma and dot_count
        else len(potential_characters)
    )
    # Calculate the final number by multiplying with powers of ten
    return sum(int(j) * 10 ** (separator_position - i - 1) for i, j in enumerate(potential_characters) if j.isdecimal())


def lurid_detection(to_check: str) -> str:
    """
    Check if a string contains a lurid title

    If the calculated score exceeds a given limit, if so, replace lurid content with not so lurid ones

    Parameters
    ----------
    to_check : str
        The string to check

    Returns
    -------
    str
        The proper string with no lurid information
    """
    score: float = 0
    lurid_symbols: List[str] = Settings.lurid_settings.other_lurid_symbols
    # Check for forbidden symbols
    score += sum(0.2 * to_check.count(i) for i in lurid_symbols)
    # Check for characters occurring multiple times
    if to_check.count('!') > 2:
        score += 0.1 * to_check.count('!')
    if to_check.count('.') > 2:
        score += 0.1 * to_check.count('.')
    # Calculate a uppercase score and store it into a certain list
    upper_list: List[Tuple[str, int]] = [
        (i, 3.5 if sum(1 for j in i if j.isupper()) / len(i) >= Settings.lurid_settings.uppercase_ratio else 0)
        for i in to_check.split()
    ]
    score += round(sum(i[1] for i in upper_list) / len(to_check.split()), 1)
    # Calculate a score based on large numbers
    numbers: Tuple[float] = tuple(number_interpretation(i) for i in to_check.split())
    score += sum(round(log10(i) / 4, 1) for i in numbers if i)
    # Act if the total score is above the threshold
    if score > Settings.lurid_settings.lurid_score_threshold:
        # Put a lurid warning in front of everything
        output: str = '!LURID! '
        # Lowercase all characters whose uppercase ratio is above the allowed ratio
        to_progress: str = ' '.join(i[0].lower() if i[1] else i[0] for i in upper_list)
        to_progress = ' '.join(
            str(int(numbers[i] / 10)) if j.isdecimal() and numbers[i] >= 100
            else str(int(numbers[i] / 10)) + ''.join(k for k in j if not (k.isdecimal() or k in ('.', ',')))
            if numbers[i] >= 100
            else j
            for i, j in enumerate(to_progress.split())
        )
        # Remove forbidden symbols
        to_progress = ''.join(i for i in to_progress if i not in lurid_symbols)
        # Remove multiple occurrences of certain characters
        while '!!' in to_progress:
            to_progress = to_progress.replace('!!', '!')
        while '..' in to_progress:
            to_progress = to_progress.replace('..', '.')
        output += to_progress
    else:
        # Don't treat the string
        output: str = to_check
    # Replace some html characters
    output = output.replace('&amp;', '&')
    output = output.replace('&#39;', "'")
    output = output.replace('&quot;', '"')
    return output
