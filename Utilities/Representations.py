from collections import UserList
from inspect import Signature, signature
from typing import List

from BTrees.IOBTree import IOBTree
from BTrees.OOBTree import OOBTree


class GenericRepr:
    def __str__(self):
        return repr(self)

    def __repr__(self, ignored_keywords: List[str] = None, explicit_keywords: List[str] = None):
        newlines = False
        newline_or_space = '\n' if newlines else ' '
        newline_or_nothing = '\n' if newlines else ''
        string = f'{self.__class__.__name__}({newline_or_nothing}'
        dir = []
        self_signature: Signature = signature(self.__init__)
        ignored_keywords = (ignored_keywords or []) + ['self']
        for i in self_signature.parameters:
            if i not in ignored_keywords:
                dir.append(i)
        if explicit_keywords:
            for i in explicit_keywords:
                if i not in self_signature:
                    dir.append(i)
        if dir:
            for i in dir:
                if i in ('no_like',):
                    # ignore all fields described in this list
                    continue
                string += f'{"    " if newlines else ""}{i}='
                lines = repr(self.__getattribute__(i)).splitlines()
                string += f'{lines.pop(0)}{newline_or_space}'
                for i in lines:
                    string += f'{"    "}{i}{newline_or_space}'
                string = f'{string[:-1]},{newline_or_space}'
            string = string[:-1]
        return f'{string[:-1]}{newline_or_nothing})'


class OOBTreeRepr(OOBTree):
    def __str__(self):
        return repr(self)

    def __repr__(self):
        return (
                f'{self.__class__.__name__}(' '{'
                + ', '.join(f'{i}: {j}' for i, j in self.items())
                + '})'
        )


class ListRepr(UserList):
    def __str__(self):
        return repr(self)

    def __repr__(self):
        return (
                f'{self.__class__.__name__}(['
                + ', '.join(i for i in self)
                + '])'
        )


class IOBTreeRepr(IOBTree):
    def __str__(self):
        return repr(self)

    def __repr__(self):
        return (
                f'{self.__class__.__name__}(' '{'
                + ', '.join(f'{i}: {j}' for i, j in self.items())
                + '})'
        )
