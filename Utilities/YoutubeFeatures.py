from pathlib import Path
from threading import Lock
from time import sleep
from typing import TypedDict, List

from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build, Resource
from googleapiclient.errors import HttpError

from Utilities.Settings import Youtube, Settings

lock: Lock = Lock()
_youtube = None

scopes = ["https://www.googleapis.com/auth/youtube"]


def authenticate_with(file: Path) -> Resource:
    """
    Get a Resource object from a client_secrets.json file

    Parameters
    ----------
    file : Path
        The location of the client_secrets.json file

    Returns
    -------
    Resource
    """
    # Get credentials and create an API client
    flow = InstalledAppFlow.from_client_secrets_file(str(file), scopes)
    with lock, Settings.db.transaction() as t:
        credentials: Credentials = t.root.credentials.get_credentials(flow, file)
    return build('youtube', 'v3', credentials=credentials)


def get_youtube() -> Resource:
    """
    Get a Resource object and if none are available, generate one

    Returns
    -------
    Resource
    """
    global _youtube
    while _youtube is None:
        try:
            _youtube = authenticate_with(Youtube.oauth_file())
        except RuntimeError as err:
            if err.args[0] == 'No key entered':
                sleep(5)
    return _youtube


def overwrite_youtube(new_youtube: Resource):
    """
    Write the youtube resource to a new Resource

    Parameters
    ----------
    new_youtube : Resource
        The new resource

    Returns
    -------
    None
    """
    global _youtube
    _youtube = new_youtube


def container(request):
    """
    Try to execute a request and if failed, use a new authentication

    Parameters
    ----------
    request
        The request to execute

    Returns
    -------
    False or dict
        False if you need to generate the request again, if successful, a dict

    Raises
    ------
    ConnectionRefusedError
        If all API-Keys are used
    """
    try:
        return request.execute()
    except HttpError as err:
        if err.resp['status'] == 403:
            if len(Youtube.oauth_files) == 1:
                raise ConnectionRefusedError('No valid API-Key left')
            with lock:
                Youtube.cycle_oauth_file()
            overwrite_youtube(authenticate_with(Youtube.oauth_file()))
            return False
        else:
            raise


class Thumbnail(TypedDict):
    url: str


class Thumbnails(TypedDict):
    maxres: Thumbnail


class MinimalSnippet(TypedDict):
    liveBroadcastContent: str
    thumbnails: Thumbnails


class LiveStreamingDetails(TypedDict):
    actualStartTime: str
    actualEndTime: str
    scheduledStartTime: str
    scheduledEndTime: str
    concurrentViewers: int
    activeLiveChatId: str


class LivestreamingResult(TypedDict, total=False):
    liveStreamingDetails: LiveStreamingDetails


class MinimalVideoResult(LivestreamingResult):
    snippet: MinimalSnippet


class NormalSnippet(MinimalSnippet):
    publishedAt: str
    channelId: str
    title: str
    description: str


class NormalStatistics(TypedDict):
    viewCount: int
    likeCount: int
    dislikeCount: int


class NormalVideoResult(LivestreamingResult):
    snippet: NormalSnippet
    kind: str
    id: str
    statistics: NormalStatistics


minimal_video_part: str = 'snippet,liveStreamingDetails'
minimal_video_fields: str = 'items(snippet(liveBroadcastContent,thumbnails(maxres(url))),liveStreamingDetails)'
normal_video_part: str = 'snippet,liveStreamingDetails,statistics'
normal_video_fields: str = 'items(kind,id,snippet(publishedAt,channelId,title,description,thumbnails(maxres(url)),' \
                           'liveBroadcastContent),liveStreamingDetails,statistics(viewCount,likeCount,dislikeCount))'
normal_video_search_fields: str = 'items(kind,id,snippet(publishedAt,channelId,title,description,' \
                                  'thumbnails(maxres(url)),liveBroadcastContent))'
normal_video_search_rest: str = 'items(kind,id,liveStreamingDetails,statistics(viewCount,likeCount,dislikeCount))'


def search_for_video_by_keyword(search_string: str, max_results: int = 10) -> List[NormalVideoResult]:
    """
    Search for a youtube video by keyword

    Parameters
    ----------
    search_string : str
        The search string
    max_results : int
        The maximum amount of search results

    Returns
    -------
    list of NormalVideoResult
    """
    while True:
        youtube = get_youtube()
        request = youtube.search().list(
            part='snippet',
            fields=normal_video_search_fields,
            maxResults=max_results,
            q=search_string,
            type="video,stream"
        )
        result = container(request)['items']
        if result:
            for i, j in enumerate(result):
                while True:
                    id_request = youtube.videos().list(
                        part='liveStreamingDetails,statistics',
                        fields=normal_video_search_rest,
                        id=j['id']['videoId'],
                        maxResults=1
                    )
                    id_result = container(id_request)['items'][0]
                    if id_result:
                        result[i] = {**result[i], **id_result}
                        result[i]['snippet']['publishedAt'] = result[i]['snippet']['publishedAt'].replace('Z', '+00:00')
                        break
            return result


def search_for_video_by_id(vid: str) -> NormalVideoResult:
    """
    Search for a video by a given video id

    Parameters
    ----------
    vid : str
        The vid to search for

    Returns
    -------
    NormalVideoResult
    """
    if vid.startswith('yt:video:'):
        vid = vid.split(':')[2]
    while True:
        youtube = get_youtube()
        request = youtube.videos().list(
            part=normal_video_part,
            fields=normal_video_fields,
            id=vid,
            maxResults=1
        )
        result = container(request)['items'][0]
        if result:
            result['snippet']['publishedAt'] = result['snippet']['publishedAt'].replace('Z', '+00:00')
            return result


def search_for_video_by_id_minimal(vid: str) -> MinimalVideoResult:
    """
    Search for a video by a given video id

    Parameters
    ----------
    vid : str
        The vid to search for

    Returns
    -------
    MinimalVideoResult
    """
    if vid.startswith('yt:video:'):
        vid = vid.split(':')[2]
    while True:
        youtube = get_youtube()
        request = youtube.videos().list(
            part=minimal_video_part,
            fields=minimal_video_fields,
            id=vid,
            maxResults=1
        )
        result = container(request)['items'][0]
        if result:
            return result


class ChannelSnippet(TypedDict):
    title: str
    description: str


class ChannelResult(TypedDict):
    id: str
    snippet: ChannelSnippet


channel_part: str = 'snippet'
channel_fields: str = 'items(id,snippet(title,description))'


def search_for_channel_by_keyword(search_string: str, max_results: int = 10) -> List[ChannelResult]:
    """
    Search for a youtube channel by keyword

    Parameters
    ----------
    search_string : str
        The search string
    max_results : int
        The maximum amount of search results

    Returns
    -------
    list of ChannelResult
    """
    while True:
        youtube = get_youtube()
        request = youtube.search().list(
            part=channel_part,
            fields=channel_fields,
            maxResults=max_results,
            q=search_string,
            type="channel"
        )
        result = container(request)['items']
        if result:
            for i in result:
                i['kind'] = i['id']['kind']
                i['id'] = i['id']['channelId']
            return result


def search_for_channel_by_id(cid: str) -> ChannelResult:
    """
    Search for a video by a given channel id

    Parameters
    ----------
    cid : str
        The cid to search for

    Returns
    -------
    ChannelResult
    """
    if cid.startswith('yt:channel:'):
        cid = cid.split(':')[2]
    while True:
        youtube = get_youtube()
        request = youtube.channels().list(
            part=channel_part,
            fields=channel_fields,
            id=cid,
            maxResults=1
        )
        result = container(request)['items'][0]
        if result:
            return result


def like_video(vid: str, count: int):
    """
    Like a youtube video

    Parameters
    ----------
    vid : str
        The id of the video
    count : int
        The number of likes, negative for dislikes

    Returns
    -------
    None
    """
    if vid.startswith('yt:video:'):
        vid = vid.split(':')[2]
    for i in range(min(abs(count), len(Youtube.oauth_files))):
        l_youtube = authenticate_with(Youtube.oauth_files[i])
        request = l_youtube.videos().rate(
            id=vid,
            rating='like' if count > 0 else 'dislike'
        )
        try:
            return request.execute()
        except HttpError as err:
            if err.resp['status'] != 403:
                raise
