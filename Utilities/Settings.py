import re
from pathlib import Path
from threading import Lock, Thread
from typing import ClassVar, List, get_type_hints, Tuple

from ZODB import DB
from ruamel import yaml

lock = Lock()


def indent(to_indent: str):
    return '\n'.join(f'{" " * 4}{i}' for i in to_indent.split('\n'))


class Repr:
    def __init__(self):
        raise RuntimeError('Tried to instantiate Options class')

    @classmethod
    def __str__(cls):
        return '\n'.join(f'{i} =\n{indent(getattr(cls, i).__str__())}' if isinstance(getattr(cls, i), Repr)
                         else f'{i}: {j} = {getattr(cls, i)}' for i, j in get_type_hints(cls).items())

    @classmethod
    def __repr__(cls):
        return cls.__str__()

    @classmethod
    def get_name(cls) -> str:
        return cls.__name__


class LuridSettings(Repr):
    uppercase_ratio: ClassVar[float] = 0.75
    lurid_score_threshold: ClassVar[float] = 1.5
    other_lurid_symbols: ClassVar[List[str]] = ['😡', '⚠️', '😱', '😂', '🙊']


class General(Repr):
    storage_location: ClassVar[Path] = Path('~', 'Downloads', 'Videos').expanduser()
    download_threads: ClassVar[int] = 1
    downloads_started: ClassVar[bool] = True
    internet_check: ClassVar[bool] = True
    video_locations: ClassVar[List[Path]] = [storage_location]
    normalize_audio: bool = False


class Youtube(Repr):
    youtube_dl_command: str = (
        'youtube-dl -f "bestvideo[height<=1080]+bestaudio/best[height<=1080]"'
        ' --write-thumbnail --write-description -c --all-subs'
    )
    timeout: int = 10 * 60
    oauth_files: List[Path] = []
    update_entries: bool = True
    current_oauth_file: int = 0

    @classmethod
    def oauth_file(cls) -> Path:
        if not cls.oauth_files:
            raise RuntimeError('No oauth file added')
        return cls.oauth_files[cls.current_oauth_file]

    @classmethod
    def cycle_oauth_file(cls):
        with lock:
            if len(cls.oauth_files) > 1:
                Settings.dirty_bit = True
                if cls.current_oauth_file + 1 < len(cls.oauth_files):
                    cls.current_oauth_file += 1
                else:
                    cls.current_oauth_file = 1

    @classmethod
    def like_accounts(cls) -> Tuple[bool, ...]:
        key_count: int = len(cls.oauth_files)
        return (True,) * (key_count - 1) + (False,) * (5 - key_count)


class Settings(Repr):
    dirty_bit: ClassVar[bool] = False
    db_channel_dirty_bit: ClassVar[bool] = False
    quitting: ClassVar[bool] = False
    lurid_settings: LuridSettings = LuridSettings
    general: General = General
    youtube: Youtube = Youtube
    config_file: ClassVar[Path] = Path('~', '.config', 'yint', 'config.yaml').expanduser()
    db: DB
    threads: List[Thread] = []

    @classmethod
    def read(cls):
        if not cls.config_file.is_file():
            cls.write()
        else:
            pattern = re.compile(r'(?<!^)(?=[A-Z])')
            with cls.config_file.open() as f:
                for ikey, ivalue in yaml.load(f, Loader=yaml.RoundTripLoader).items():
                    attr = getattr(cls, pattern.sub('_', ikey).lower())
                    for jkey, jvalue in ivalue.items():
                        if isinstance(getattr(attr, jkey.lower()), Path):
                            setattr(attr, jkey.lower(), Path(jvalue))
                        else:
                            setattr(attr, jkey.lower(), jvalue)

    @classmethod
    def write(cls):
        with cls.config_file.open('w') as f:
            config = {}
            for i in (cls.lurid_settings, cls.general, cls.youtube):
                config[i.get_name()] = {j: str(getattr(i, j)) if isinstance(getattr(i, j), Path)
                else [str(k) for k in getattr(i, j)] if isinstance(getattr(i, j), list)
                                                        and getattr(i, j) and isinstance(getattr(i, j)[0], Path)
                else getattr(i, j) for j in get_type_hints(i).keys()}
            yaml.dump(config, f, width=80, indent=4, Dumper=yaml.RoundTripDumper)
