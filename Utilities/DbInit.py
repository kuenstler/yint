from pathlib import Path

from ZODB import DB

from DB.ChannelList import ChannelListStorage
from DB.DownloadList import DownloadListStorage
from DB.OauthCredentials import CredentialsStorage
from DB.PingList import PingListStorage
from DB.StreamList import StreamListStorage
from DB.VideoList import VideoListStorage
from .Settings import Settings


def init():
    parent: Path = Settings.config_file.parent
    parent_parent: Path = parent.parent
    if not parent_parent.is_dir():
        parent_parent.mkdir()
    if not parent.is_dir():
        parent.mkdir()
    Settings.read()
    Settings.db = DB(str(parent / 'database.db'), pool_size=20)
    with Settings.db.transaction() as conn:
        if not hasattr(conn.root, 'videos'):
            conn.root.videos = VideoListStorage()
        if not hasattr(conn.root, 'streams'):
            conn.root.streams = StreamListStorage()
        if not hasattr(conn.root, 'channels'):
            conn.root.channels = ChannelListStorage()
        if not hasattr(conn.root, 'download_list'):
            conn.root.download_list = DownloadListStorage()
        if not hasattr(conn.root, 'pings'):
            conn.root.pings = PingListStorage()
        if not hasattr(conn.root, 'credentials'):
            conn.root.credentials = CredentialsStorage()
