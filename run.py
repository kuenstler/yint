#! /usr/bin/env python
from threading import Lock

from Helpers.DownloadRepeat import DownloadRepeat
from Helpers.OnlineCheck import OnlineCheck
from Helpers.PingRepeat import PingRepeat
from Helpers.StreamRepeat import StreamRepeat
from Helpers.YoutubeSyncRepeat import ChannelSyncRepeat
from TUI.TUI import run
from Utilities.DbInit import init
from Utilities.Settings import Settings

lock: Lock = Lock()

if __name__ == '__main__':
    init()
    with lock:
        Settings.threads.append(DownloadRepeat())
        Settings.threads.append(PingRepeat())
        Settings.threads.append(ChannelSyncRepeat())
        Settings.threads.append(StreamRepeat())
        Settings.threads.append(OnlineCheck())
        # This is a bit more complex to avoid iterating over new objects added by threads
        [Settings.threads[i].start() for i in range(len(Settings.threads))]
    try:
        run()
    except KeyboardInterrupt:
        pass
    finally:
        with lock:
            Settings.quitting = True
        if Settings.dirty_bit:
            Settings.write()
        [i.join() for i in Settings.threads if not i.daemon]
        Settings.db.close()
