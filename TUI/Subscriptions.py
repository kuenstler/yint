from threading import Lock
from webbrowser import open_new_tab

from ZODB.Connection import Connection
from transaction import TransactionManager

from DB.Channel import ChannelStorage
from DB.ChannelList import ChannelListStorage
from Utilities.Settings import Settings
from .OwnUrwid.GenericList import GenericList
from .Runtime import Runtime
from .UsualQuestions import unsubscribe_creator, view_in_browser

lock: Lock = Lock()


class SubscriptionsList(GenericList):
    def __init__(self):
        """
        A list of subscribed channels
        add_button needs 1 config argument
        (can_view_videos)
        """
        self._tm: TransactionManager = TransactionManager()
        self._conn: Connection = Settings.db.open(self._tm)
        super().__init__(
            ('Subscriptions', 'Platform'),
            (
                unsubscribe_creator[:2],
                view_in_browser,
                ('View videos from channel', 'i', 0)
            )
        )

    def answer(self, reply_id: str, title: str, entry_id: str):
        """
        The method which get's confronted with the result of the ButtonBox

        Override this if you want to implement your own behaviour
        Parameters
        ----------
        reply_id : str
            The id of the answer code
        title : str
            The title of the MessageBox
        entry_id : str
            The id of the attached entry

        Returns
        -------
        None
        """
        with lock:
            self._conn.sync()
            channel: ChannelStorage = self._conn.root.channels[entry_id]
            if reply_id == view_in_browser[1]:
                open_new_tab(channel.url)
            elif reply_id == unsubscribe_creator[1]:
                channel.subscribed = not channel.subscribed
            self._tm.commit()


def subscriptions(*args):
    """
    Initializes SubscriptionsList and overwrites the main frames body

    Parameters
    ----------
    args
        Unused

    Returns
    -------
    None
    """
    Runtime.frame.body = SubscriptionsList()
    channels: ChannelListStorage = Runtime.conn.root.channels
    [Runtime.frame.body.add_button(*i) for i in channels.subscription_items]
