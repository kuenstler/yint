from pathlib import Path

import urwid

from Utilities.Settings import Settings
from .OwnUrwid.PrettyButton import PrettyButton
from .OwnUrwid.QuestionBox import ButtonBox, CSVBox, List, MessageBox
from .Runtime import Runtime

DOWNLOAD, DREPLY, IREPLY, SREPLY = ('download', 'd', 'i', 's')


class SettingsWindow(urwid.ListBox):
    def __init__(self):
        """
        A ListBox showing all options

        Returns
        -------
        SettingsWindow
        """
        walker = urwid.SimpleFocusListWalker([
            PrettyButton('Manage Downloads', self.download_question),
            PrettyButton('Reload local video DB'),
            PrettyButton('Local video locations', self.video_locations),
            PrettyButton('Exit Yint', on_press=self.exit_button)
        ])
        super().__init__(walker)

    def video_locations(self, *args):
        """
        Spawns a CSVBox which lets the user change the Video search locations

        Parameters
        ----------
        args : tuple of any and str
            The first item is the button itself, and the second one is the attached id

        Returns
        -------
        None
        """
        CSVBox(
            'Video Locations',
            self.video_set,
            self.video_check,
            subtitle='Set file paths where Yint will search for known videos',
            allow_cancel=True,
            existing_text=',\n'.join(str(i) for i in Settings.general.video_locations)
        )

    @staticmethod
    def video_check(video: str) -> bool:
        """
        Checks whether a given video path is a valid directory

        Parameters
        ----------
        video : str
            Video directory path

        Returns
        -------
        bool
        """
        return Path(video).expanduser().is_dir()

    @staticmethod
    def video_set(text: List[str]):
        """
        Sets new video search paths to Settings

        Parameters
        ----------
        text : list of str
            The paths

        Returns
        -------
        None

        Notes
        -----
        The video paths will get converted to Path objects
        """
        Settings.general.video_locations = [Path(i).expanduser().absolute() for i in text]
        Settings.dirty_bit = True

    def download_question(self, *args):
        """
        Asks the user how to proceed with new videos

        Parameters
        ----------
        args : tuple of any and str
            The first item is the button itself, and the second one is the attached id

        Returns
        -------
        None
        """
        ButtonBox('Do you want to download (new) videos?', [
            ('Download videos', DREPLY),
            ('Download videos without internet check', IREPLY),
            ('Stop downloading videos', SREPLY)
        ], self.download_reply)

    @staticmethod
    def download_reply(reply_id: str, title: str, entry_id: str):
        """
        Processes the result of the download_question

        Parameters
        ----------
        reply_id : str
            The id of the answer code
        title : str
            The title of the MessageBox
        entry_id : str
            The id of the attached entry

        Returns
        -------
        None
        """
        if reply_id == DREPLY and (Settings.general.downloads_started is not True
                                   or Settings.general.internet_check is not True):
            Settings.general.downloads_started = True
            Settings.general.internet_check = True
            Settings.dirty_bit = True
        elif reply_id == IREPLY and (Settings.general.downloads_started is not True
                                     or Settings.general.internet_check is not False):
            Settings.general.downloads_started = True
            Settings.general.internet_check = False
            Settings.dirty_bit = True
        elif reply_id == SREPLY and Settings.general.downloads_started is not False:
            Settings.general.downloads_started = False
            Settings.dirty_bit = True

    def exit_button(self, *args):
        """
        Asks the user if he really wants to exit

        Parameters
        ----------
        args : unknown
            Unused

        Returns
        -------
        None
        """
        MessageBox('Do you rally want to exit the bot', 'This will stop all running downloads', allow_cancel=True,
                   command=self.exit_reaction)

    @staticmethod
    def exit_reaction(reaction: bool):
        """
        Exits the program if the user told it to do so

        Parameters
        ----------
        reaction : bool
            The reaction from the exit MessageBox

        Returns
        -------
        None
        """
        if reaction:
            raise urwid.ExitMainLoop


def settings():
    """
    Sets a SettingsWindow as fame body

    Returns
    -------
    None
    """
    Runtime.frame.body = SettingsWindow()
