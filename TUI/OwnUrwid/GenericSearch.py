from abc import abstractmethod
from typing import Tuple, Optional, Iterable

import urwid

from Utilities.Typings import AddButtonArguments
from .EasyKeyword import register_key
from .GenericList import GenericList
from .NavigateableFrame import NavigateableFrame
from .PrettyButton import PrettyButton
from .TabMixin import TabColumnsMixin
from .Utilities import QuestionAnswers, Markup

SearchEntries = Iterable[AddButtonArguments]


class GenericSearch(NavigateableFrame):
    def __init__(self, main_title: Markup, titles: Tuple[str, ...], questions: QuestionAnswers):
        """
        A generic search for various purposes, used mostly the same way as a GenericList

        Especially when preparing the return list, this search_for method needs to follow exactly GenericLists
        add_button arguments

        Parameters
        ----------
        main_title : Markup
            The title to appear before the text entry
        titles : tuple of str
            Titles for each column, has to be a tuple
        questions : QuestionAnswers
            The answers to individual videos like in GenericList

        See Also
        --------
        .GenericList : most of the functionality is stolen from there
        """
        self.edit: urwid.Edit = urwid.Edit(caption=main_title + ' -> ')
        self.titles: Tuple[str, ...] = titles
        self.questions: QuestionAnswers = questions
        search_button: PrettyButton = PrettyButton('Search', self.search)
        super().__init__(urwid.SolidFill(), header=TabColumnsMixin([self.edit, (10, search_button)]))
        self.focus_part = 'header'

    @abstractmethod
    def search_for(self, text: str) -> SearchEntries:
        """
        Search for a given text.

        Parameters
        ----------
        text : str
            The text to search for

        Returns
        -------
        SearchEntries
            An iterable of add_buttons

        See Also
        --------
        .GenericList.add_button : The syntax for each entry
        """
        raise NotImplementedError(f'search_for is not implemented in {self.__class__.__name__}')

    def search(self, *args):
        """
        The actual search, processes the input and prints it out

        Parameters
        ----------
        args : unknown
            Unused

        Returns
        -------
        None
        """
        entries: SearchEntries = self.search_for(self.edit.get_edit_text())
        self.body = GenericList(self.titles, self.questions, answer_func=self.answer)
        for i in entries:
            self.body.add_button(*i)
        self.focus_part = 'body'

    @register_key('enter')
    def key_enter(self, key) -> Optional[bool]:
        """
        Added a behaviour if enter is pressed

        Parameters
        ----------
        key : str
            The pressed key

        Returns
        -------
        bool, optional
            Whether the key has been handled
        """
        self.search()
        return True

    def answer(self, reply_id: str, title: str, entry_id: str):
        """
        The method which get's confronted with the result of the ButtonBox

        Override this if you want to implement your own behaviour
        Parameters
        ----------
        reply_id : str
            The id of the answer code
        title : str
            The title of the MessageBox
        entry_id : str
            The id of the attached entry

        Returns
        -------
        None
        """
        raise NotImplementedError(f'{self.__class__.__name__} has no answer method')
