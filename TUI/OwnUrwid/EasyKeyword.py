from typing import Dict, Callable, Tuple, Optional

import urwid


class EasyKeywordMixin:
    def __init__(self, *args, **kwargs):
        """
        Required mixin to use register_key

        Parses all methods of all inheriting classes and registers them in __keys

        Parameters
        ----------
        args : unknown
            Arguments for the actual Base Class
        kwargs : unknown
            Arguments for the actual base class
        """
        assert getattr(self, 'keypress') is not None, f'{self.__class__.__name__} doesn\'t has a keypress method'
        self._keys: Dict[str, Callable] = {}
        mro: Tuple[type] = self.__class__.__mro__
        own_index: int = mro.index(EasyKeywordMixin)
        assert own_index < mro.index(urwid.Widget), \
            f'Inherited from urwid before EasyKeywordMixin in {self.__class__.__name__}'
        for i in range(0, own_index):
            for j in mro[i].__dict__.values():
                if j and hasattr(j, 'key'):
                    self._keys[j.key] = j
        super().__init__(*args, **kwargs)

    def keypress(self, size, key: str) -> Optional[str]:
        """
        Handling keypress and delegates it first to inherited classes and afterwords to methods registered in __keys
        if possible

        Parameters
        ----------
        size : unknown
            Used by urwid
        key : str
            The actual key

        Returns
        -------
        str, optional
        """
        new_key: Optional[str] = super().keypress(size, key)
        if new_key and new_key in self._keys:
            if isinstance(self._keys[new_key], type(self.keypress)):  # Check whether it's a method
                if self._keys[new_key](new_key):
                    return
            else:  # If not, give self as first argument
                if self._keys[new_key](self, new_key):
                    return
        return new_key


def register_key(key: str):
    """
    Decorate methods you want to use to handle keys with this decorator

    Decorated methods should return True when they handled the keyword

    Parameters
    ----------
    key : str
        The key which should be handled by the decorated method

    Returns
    -------
    callable
        The actual decorator
    """

    def actual_decorator(f):
        f.key = key
        return f

    return actual_decorator
