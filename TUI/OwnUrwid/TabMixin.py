from abc import abstractmethod
from typing import Iterable, Optional

import urwid

from .EasyKeyword import EasyKeywordMixin


class ITabMixin(EasyKeywordMixin):
    """
    Base class for all Tab mixins

    Attributes
    ----------
    base_class : bool
        If this widget is the topmost widget

    Warnings
    --------
    You may not inherit derived classes after their urwid's complement
    """
    base_class: bool = False

    @abstractmethod
    def tab_in(self):
        """
        Will get executed if the tab visits some higher widget

        Returns
        -------
        None
        """
        raise NotImplementedError


class TabFrameMixin(ITabMixin, urwid.Frame):
    """
    A mixin which provides a tab functionality

    Attributes
    ----------
    base_class : bool
        If this widget is the topmost widget

    Warnings
    --------
    You may not inherit this after urwid.Frame
    """

    def keypress(self, size, key):
        """
        Added a behaviour if tab is unhandled

        Parameters
        ----------
        size
            Idk
        key : str
            The actual key

        Returns
        -------
        None or str
        """

        def __set_focus(part: str):
            """
            Set the focus to a given part, also checks if the focused item is a ITabMixin and executes tab_in if needed

            Parameters
            ----------
            part : {'header', 'body', 'footer'}
                The part to set the focus to

            Returns
            -------
            None
            """
            self.focus_position = part
            if isinstance(self.focus, ITabMixin):
                self.focus.tab_in()

        new_key = super().keypress(size, key)
        if new_key == 'tab':
            if self.focus_position == 'header':
                if self.body.selectable():
                    __set_focus('body')
                    return
                elif self.footer and self.footer.selectable():
                    __set_focus('footer')
                    return
            elif self.focus_position == 'body':
                if self.footer and self.footer.selectable():
                    __set_focus('footer')
                    return
                elif self.base_class and self.header and self.header.selectable():
                    __set_focus('header')
                    return
            elif self.focus_position == 'footer' and self.base_class:
                if self.header and self.header.selectable():
                    __set_focus('header')
                    return
                elif self.body.selectable():
                    __set_focus('body')
                    return
        return new_key

    def tab_in(self):
        """
        Will get executed if the tab visits some higher widget

        Returns
        -------
        None
        """
        if self.header and self.header.selectable():
            self.focus_position = 'header'
        elif self.body.selectable():
            self.focus_position = 'body'
        elif self.footer and self.footer.selectable():
            self.focus_position = 'footer'
        if isinstance(self.focus.base_widget, ITabMixin):
            self.focus.base_widget.tab_in()


class TabListMixin(ITabMixin):
    """
    A mixin which provides a tab functionality for list like widgets

    Attributes
    ----------
    base_class : bool
        If this widget is the topmost widget
    """

    @staticmethod
    def __get_first_occurrence(iterable: Iterable[bool]) -> Optional[int]:
        """
        Get the first occurrence of True in an iterable

        Parameters
        ----------
        iterable : iterable of bool
            The iterable to check

        Returns
        -------
        int, optional
            The first occurrence, None if no occurrence was found
        """
        pos: int = 0
        for i in iterable:
            if i:
                return pos
            pos += 1

    def __set_focus(self, position: int):
        """
        Set the focus to a given position, also checks if the focused item is a ITabMixin and executes tab_in if needed

        Parameters
        ----------
        position : int
            The position to focus

        Returns
        -------
        None
        """
        self.focus_position = position
        if isinstance(self.focus.base_widget, ITabMixin):
            self.focus.base_widget.tab_in()

    def __search_and_set(self, start: int = 0):
        """
        Searches for the next selectable item after start

        Parameters
        ----------
        start : int
            The first entry to search for

        Returns
        -------
        bool, optional
            Returns True if an entry was found
        """
        if self.focus_position == 0 and len(self.contents) <= 1:
            return
        next_pos: Optional[int] = self.__get_first_occurrence(
            (i[0] if isinstance(i, tuple) else i).selectable() for i in self.contents[start:]
        )
        if next_pos is None:
            return
        self.__set_focus(next_pos + start)
        return True

    def keypress(self, size, key):
        """
        Added a behaviour if tab is unhandled

        Parameters
        ----------
        size
            Idk
        key : str
            The actual key

        Returns
        -------
        None or str
        """
        new_key = super().keypress(size, key)
        if not self.contents:
            return new_key
        if new_key == 'tab':
            if self.__search_and_set(self.focus_position + 1):
                return
        return new_key

    def tab_in(self):
        """
        Will get executed if the tab visits some higher widget

        Returns
        -------
        None
        """
        self.__search_and_set()


class TabPileMixin(TabListMixin, urwid.Pile):
    """
    A mixin which provides a tab functionality for urwid.Pile widgets

    Attributes
    ----------
    base_class : bool
        If this widget is the topmost widget
    """


class TabColumnsMixin(TabListMixin, urwid.Columns):
    """
    A mixin which provides a tab functionality for urwid.Columns widgets

    Attributes
    ----------
    base_class : bool
        If this widget is the topmost widget
    """
