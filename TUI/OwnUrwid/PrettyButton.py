from itertools import chain
from typing import Callable, Optional, Union, Tuple

import urwid

from .Utilities import Markup

forbidden: str = ''.join(chr(i) for i in chain(range(0xfe00, 0xfe10), range(0xc0, 0xc2), range(0xf5, 0x100)))


def remove_forbidden_from_string(string: str) -> str:
    """
    Remove VARIATION SELECTOR-1 to VARIATION SELECTOR-16

    Parameters
    ----------
    string : str
        The string to clean

    Returns
    -------
    str
        Clean string
    """
    return ''.join(i for i in string if i not in forbidden)


def remove_forbidden_from_tuple_or_string(to_clean: Union[Tuple[str, str], str]) -> Union[Tuple[str, str], str]:
    """
    Remove VARIATION SELECTOR-1 to VARIATION SELECTOR-16

    Parameters
    ----------
    to_clean : str or tuple of str and str
        The string or tuple to clean

    Returns
    -------
    str
        Clean string or tuple
    """
    if isinstance(to_clean, str):
        return remove_forbidden_from_string(to_clean)
    elif isinstance(to_clean, tuple):
        return to_clean[0], remove_forbidden_from_string(to_clean[1])


class PrettyButton(urwid.Button):
    def __init__(self, text: Markup, on_press: Callable = None, user_data: str = None, markup: str = None,
                 highlighting: bool = False, left: Markup = '[', right: Markup = ']'):
        """
        A Button with much more options

        Parameters
        ----------
        text : Markup
            Caption of the Button
        on_press : callable
            The method to execute if the Button is pressed
        user_data : str
            Additional information given to the callback command
        markup : str
            markup used for highlighting
        highlighting : bool
            Whether or not to highlight the first occurrence of user_data, see the warnings
        left : Markup
            The left part of the Button outside of the caption
        right : Markup
            The right part of the Button outside of the caption

        Warnings
        --------
        If you want to use highlighting, you need to supply a markup
        If you want to use highlighting, you may not use a list as text
        If you want to use highlighting, user_data needs to be 1 character long
        """
        self.button_left = urwid.Text(left)
        self.button_right = urwid.Text(right)
        self.user_data: Optional[str] = user_data
        if isinstance(text, (str, tuple)):
            text = remove_forbidden_from_tuple_or_string(text)
        elif isinstance(text, list):
            text = [remove_forbidden_from_tuple_or_string(i) for i in text]
        else:
            raise RuntimeError(f'Unknown markup structure {markup}')
        highlight_markup = f'{markup}_highlight'
        if highlighting and len(user_data) == 1:
            if not markup:
                raise RuntimeError(f'Tried to highlight without markup given')
            if isinstance(text, tuple):
                new_text: str = text[1]
            elif isinstance(text, str):
                new_text: str = text
            else:
                raise RuntimeError(f'Highlighting text only works on unformatted text')
            is_key = user_data.upper() in new_text.upper()
            if is_key:
                index = new_text.upper().index(user_data.upper())
                text: Markup = [
                    (markup, text[:index]) if isinstance(text, str) else (text[0], text[1][:index]),
                    (highlight_markup, new_text[index]),
                    (markup, text[index + 1:]) if isinstance(text, str) else (text[0], text[1][index + 1:]),
                ]
        super().__init__(text, on_press, user_data)


class FocusPrettyButton(urwid.AttrMap):
    def __init__(self, text: Markup, on_press: Callable = None, user_data: str = None, markup: str = None,
                 highlighting: bool = False, left: str = '[', right: str = ']'):
        """
        A Button which changes it's palette entry while on focus

        See Also
        --------
        PrettyButton : For an explanation of parameters
        """
        if not markup:
            raise RuntimeError('Tried to focus without markup given')
        focus_markup = f'{markup}_focus'
        highlight_markup = f'{markup}_highlight'
        highlight_focus_markup = f'{markup}_highlight_focus'
        button = PrettyButton(text, on_press=on_press, user_data=user_data, markup=markup, highlighting=highlighting,
                              left=left, right=right)
        super().__init__(button, {markup: markup, highlight_markup: highlight_markup},
                         {markup: focus_markup, highlight_markup: highlight_focus_markup})
