from typing import Dict, Tuple, Callable, Optional

import urwid

from TUI.Runtime import Runtime
from .EasyLineBox import EasyLineBox
from .PrettyButton import PrettyButton
from .QuestionBox import ButtonBox
from .TabMixin import TabFrameMixin
from .Utilities import QuestionAnswers


class GenericList(TabFrameMixin, urwid.Frame):
    """
    GenericList

    Attributes
    ----------
    questions : QuestionAnswers
        Questions to be passed on to ButtonBox
    list : urwid.SimpleFocusListWalker
        The items list
    text : urwid.Text
        The Text of the item count
    configuration : dict of str and tuple of bools
        Configuration for entries

    Methods
    -------
    __init__
    add_button
    answer
    """

    def __init__(self, titles: Tuple[str, ...], questions: QuestionAnswers, right: bool = False, left: bool = False,
                 answer_func: Callable = None):
        """
        A generic list with a title and a ListBox body surrounded by thick and thin lines

        right and left indicate whether this widget has a right or left neighbour.

        Parameters
        ----------
        titles : tuple of str
            Title of the List
        questions : QuestionAnswers
            Questions to be passed on to ButtonBox
        right : bool
            Whether this Widget has a right neighbour
        left : bool
            Whether this Widget has a right neighbour

        Notes
        -----
        Optional questions should look like this ('question', 'q', 0)
        which accesses the first key from the configuration tuple
        you can also use negations like ('Not Question', 'q', 0, 'not')
        """
        self.questions: QuestionAnswers = questions
        self.answer_func: Optional[Callable] = answer_func
        self.text: urwid.Text = urwid.Text('0')
        list_title = EasyLineBox(
            urwid.Columns([urwid.Text(titles[0]), self.text]
                          + [urwid.Text(i) for i in titles[1:]]),
            top=True,
            left=None if left else True,
            right=True,
            right_neighbour=right,
            bottom_neighbour=True,
        )
        self.list: urwid.SimpleFocusListWalker = urwid.SimpleFocusListWalker([])
        self.len_titles: int = len(titles)
        self.configuration: Dict[str, Tuple[bool, ...]] = {}
        self.subtitles: Dict[str, str] = {}
        self.texts: Dict[str, Tuple[str, ...]] = {}
        list_widget = EasyLineBox(
            urwid.ListBox(self.list),
            top=None,
            left=None if left else True,
            bottom=True,
            right=True,
            right_neighbour=right,
        )
        super().__init__(list_widget, list_title)

    def click(self, *args):
        """
        The method which will get confronted with the result from a pressed Button / Key

        Parameters
        ----------
        args : tuple of any and str
            The first item is the button itself, and the second one is the attached id

        Returns
        -------
        None
        """
        current_tuple: Tuple[bool, ...] = self.configuration[args[1]]
        current_subtitle: str = self.subtitles[args[1]]
        questions = [i[:2] for i in self.questions if
                     len(i) == 2
                     or len(i) == 3 and current_tuple[i[2]]
                     or len(i) == 4 and i[3] == 'not' and not current_tuple[i[2]]]
        ButtonBox(args[0].label, questions, self.answer, subtitle=current_subtitle, entry_id=args[1])

    def answer(self, reply_id: str, title: str, entry_id: str):
        """
        The method which get's confronted with the result of the ButtonBox

        Override this if you want to implement your own behaviour
        Parameters
        ----------
        reply_id : str
            The id of the answer code
        title : str
            The title of the MessageBox
        entry_id : str
            The id of the attached entry

        Returns
        -------
        None
        """
        if self.answer_func:
            self.answer_func(reply_id, title, entry_id)
        else:
            Runtime.frame.footer = urwid.Text('{' f'reply_id={reply_id}, title={title}, entry_id={entry_id}' '}')

    def get_columns(self, id: str) -> urwid.Columns:
        """
        Get a Columns widget dor a given id

        Parameters
        ----------
        id : str
            The id to generate the columns for

        Returns
        -------
        urwid.Columns
            The columns
        """
        return urwid.Columns([('weight', 2, PrettyButton(self.texts[id][0], self.click, id))]
                             + [urwid.Text(self.texts[id][i]) for i in range(1, self.len_titles)])

    def add_button(self, texts: Tuple[str, ...], id: str, subtitle: str = '', configuration: Tuple[bool, ...] = ()):
        """
        Add a Button to this ListBox

        Parameters
        ----------
        texts : tuple of str
            The text to display
        id : str
            The id attached to the text
        configuration :  tuple of bool
            The configuration of optional questions
        subtitle : str
            Subtitle of the dialogue

        Returns
        -------
        None
        """
        self.configuration[id] = configuration
        self.subtitles[id] = subtitle
        self.texts[id] = texts
        self.list.append(self.get_columns(id))
        self.text.set_text(f'{len(self.list)}')

    def remove_button(self, id: str):
        """
        Removes a button with a given id

        Parameters
        ----------
        id : str
            The id of the button to remove

        Returns
        -------
        None
        """
        if id in self.configuration and id in self.subtitles:
            self.list.pop([i for i, j in enumerate(self.list) if j.contents[0][0].user_data == id][0])
            del self.texts[id]
            del self.configuration[id]
            del self.subtitles[id]
            self.text.set_text(str(int(self.text.text) - 1))
