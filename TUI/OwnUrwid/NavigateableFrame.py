from typing import Optional

from .EasyKeyword import register_key
from .TabMixin import TabFrameMixin


class NavigateableFrame(TabFrameMixin):
    """
    Frame with an updated keypress to be navigateable
    """

    @register_key('down')
    def key_down(self, key) -> Optional[bool]:
        """
        Added a behaviour if down is pressed

        Parameters
        ----------
        key : str
            The pressed key

        Returns
        -------
        bool, optional
            Whether the key has been handled
        """
        if self.focus_part == 'header' and self.body.selectable():
            self.focus_part = 'body'
        elif self.focus_part == 'body' and self.footer and self.footer.selectable():
            self.focus_part = 'footer'
        else:
            return
        self._invalidate()
        return True

    @register_key('up')
    def key_up(self, key) -> Optional[bool]:
        """
        Added a behaviour if up is pressed

        Parameters
        ----------
        key : str
            The pressed key

        Returns
        -------
        bool, optional
            Whether the key has been handled
        """
        if self.focus_part == 'body' and self.header and self.header.selectable():
            self.focus_part = 'header'
        elif self.focus_part == 'footer' and self.body.selectable():
            self.focus_part = 'body'
        else:
            return
        self._invalidate()
        return True
