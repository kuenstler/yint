from typing import Optional

import urwid

# left, right, top, bottom
# None means no connection, False means a thin connection, True means a thick connection
combinations = {
    (False, False, False, False): '┼',
    (False, False, False, True): '┃',
    (False, False, False, None): '┴',
    (False, False, True, False): '╀',
    (False, False, True, True): '╂',
    (False, False, True, None): '┸',
    (False, False, None, False): '┬',
    (False, False, None, True): '┰',
    (False, False, None, None): '─',
    (False, True, False, False): '┾',
    (False, True, False, True): '╆',
    (False, True, False, None): '┶',
    (False, True, True, False): '╄',
    (False, True, True, True): '╊',
    (False, True, True, None): '┺',
    (False, True, None, False): '┮',
    (False, True, None, True): '┲',
    (False, True, None, None): '━',
    (False, None, False, False): '├',
    (False, None, False, True): '┟',
    (False, None, False, None): '┘',
    (False, None, True, False): '┦',
    (False, None, True, True): '┨',
    (False, None, True, None): '┚',
    (False, None, None, False): '┐',
    (False, None, None, True): '┒',
    (False, None, None, None): '│',
    (True, False, False, False): '┽',
    (True, False, False, True): '╅',
    (True, False, False, None): '┵',
    (True, False, True, False): '╃',
    (True, False, True, True): '╉',
    (True, False, True, None): '┹',
    (True, False, None, False): '┭',
    (True, False, None, True): '┱',
    (True, False, None, None): '━',
    (True, True, False, False): '┿',
    (True, True, False, True): '╈',
    (True, True, False, None): '┷',
    (True, True, True, False): '╇',
    (True, True, True, True): '╋',
    (True, True, True, None): '┻',
    (True, True, None, False): '┯',
    (True, True, None, True): '┳',
    (True, True, None, None): '━',
    (True, None, False, False): '┥',
    (True, None, False, True): '┪',
    (True, None, False, None): '┙',
    (True, None, True, False): '┩',
    (True, None, True, True): '┫',
    (True, None, True, None): '┛',
    (True, None, None, False): '┑',
    (True, None, None, True): '┓',
    (True, None, None, None): '━',
    (None, False, False, False): '├',
    (None, False, False, True): '┟',
    (None, False, False, None): '',
    (None, False, True, False): '┞',
    (None, False, True, True): '┠',
    (None, False, True, None): '┖',
    (None, False, None, False): '┌',
    (None, False, None, True): '┍',
    (None, False, None, None): '─',
    (None, True, False, False): '┝',
    (None, True, False, True): '┢',
    (None, True, False, None): '',
    (None, True, True, False): '┡',
    (None, True, True, True): '┣',
    (None, True, True, None): '┗',
    (None, True, None, False): '┍',
    (None, True, None, True): '┏',
    (None, True, None, None): '━',
    (None, None, False, False): '│',
    (None, None, False, True): '┃',
    (None, None, False, None): '│',
    (None, None, True, False): '┃',
    (None, None, True, True): '┃',
    (None, None, True, None): '┃',
    (None, None, None, False): '│',
    (None, None, None, True): '┃',
    (None, None, None, None): ''
}


def get_corner(l: Optional[bool], r: Optional[bool], t: Optional[bool], b: Optional[bool]) -> str:
    """
    Returns a character for a given set of surrounding connections

    None means no connection, False means a thin connection, True means a thick connection

    Parameters
    ----------
    l : bool, optional
        The left connection
    r : bool, optional
        The right connection
    t : bool, optional
        The top connection
    b : bool, optional
        The bottom connection

    Returns
    -------
    str
        The actual character
    """
    return combinations[(l, r, t, b)]


class EasyLineBox(urwid.LineBox):
    def __init__(
            self,
            *args,
            left: Optional[bool] = False,
            right: Optional[bool] = False,
            top: Optional[bool] = False,
            bottom: Optional[bool] = False,
            left_neighbour: bool = False,
            right_neighbour: bool = False,
            top_neighbour: bool = False,
            bottom_neighbour: bool = False,
            **kwargs
    ):
        """
        A LineBox where you can specify how each border looks

        None means no connection, False means a thin connection, True means a thick connection

        Parameters
        ----------
        args : list of any
            Arguments to be given to urwid.LineBox
        left : bool, optional
            Whether to draw a left line and how it should look like
        right : bool, optional
            Whether to draw a right line and how it should look like
        top : bool, optional
            Whether to draw a top line and how it should look like
        bottom : bool, optional
            Whether to draw a bottom line and how it should look like
        left_neighbour : bool
            Whether this LineBox has a left neighbour and thus should connect it
        right_neighbour : bool
            Whether this LineBox has a right neighbour and thus should connect it
        top_neighbour : bool
            Whether this LineBox has a top neighbour and thus should connect it
        bottom_neighbour : bool
            Whether this LineBox has a bottom neighbour and thus should connect it
        kwargs : dict of str and any
            Further kwargs to be given to urwid.LineBox, overwrites settings set by this LineBox
        """
        output = {}
        for i, j in zip(('rline', 'lline'), (right, left)):
            output[i] = get_corner(None, None, j, j)
        for i, j in zip(('tline', 'bline'), (top, bottom)):
            output[i] = get_corner(j, j, None, None)

        def cont(orig, add):
            return orig if add else None

        output['tlcorner'] = get_corner(cont(top, left_neighbour), top, cont(left, top_neighbour), left)
        output['trcorner'] = get_corner(top, cont(top, right_neighbour), cont(right, top_neighbour), right)
        output['blcorner'] = get_corner(cont(bottom, left_neighbour), bottom, left, cont(left, bottom_neighbour))
        output['brcorner'] = get_corner(bottom, cont(bottom, right_neighbour), right, cont(right, bottom_neighbour))
        super().__init__(*args, **{**output, **kwargs})
