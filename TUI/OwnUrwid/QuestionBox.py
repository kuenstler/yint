from abc import abstractmethod
from typing import Callable, List, Iterable, Optional

import urwid

from TUI.Runtime import Runtime
from .EasyKeyword import EasyKeywordMixin
from .PrettyButton import PrettyButton
from .Utilities import QuestionAnswers


class QuestionBox(EasyKeywordMixin, urwid.LineBox):
    """
    QuestionBox

    Attributes
    ----------
    cancel: list of str
        List of canceling letters
    keywords: list of str
        List of all letters
    title : str
        Title of the dialogue
    """

    def __init__(self, content, title: str, allow_cancel: bool = True, cancel: Iterable[str] = (),
                 keywords: Iterable[str] = (), wrap_attr: str = None):
        """
        A basic QuestionBox

        It will get displayed above everything else using an Overlay after calling register.
        The cancel letters don't need to get passed to the keywords, because they will automatically get added.
        If wrap_attr is set it will automatically add a AttrWrap around everything using the given palette string.
        If allow_cancel is set to False, all cancel letters won't work.

        Parameters
        ----------
        content : any
            The Widget to wrap by this
        title : str
            The title of the LineBox
        allow_cancel : bool
            Whether cancel buttons work or not
        cancel : iterable str
            Letters used to cancel the dialogue, will also affect the is_cancel property of call
        keywords : iterable str
            Letters except from cancel letters, also work if allow_cancel is set to False
        wrap_attr : str
            The palette str used for an AttrWrap around content

        Returns
        -------
        QuestionBox

        See Also
        --------
        urwid.LineBox

        Notes
        -----
        When implementing Buttons in child classes, use call_button as response
        """
        if wrap_attr:
            content = urwid.AttrWrap(content, wrap_attr)
        self.cancel: List[str] = ['esc', *cancel] if allow_cancel else []
        self.keywords = [*self.cancel, *keywords]
        self.title: str = title
        super().__init__(content, self.title)
        self._keys = {i: self.pressed for i in self.keywords}

    def register(self):
        """
        Register this Widget as main Widget

        Returns
        -------
        None
        """
        Runtime.loop.widget = urwid.Overlay(
            self,
            Runtime.loop.widget,
            'center',
            ('relative', 60),
            'middle',
            ('relative', 60)
        )

    def destroy(self):
        """
        Destroy this widget as main Widget and thus release the rest again

        Returns
        -------
        None
        """
        if isinstance(Runtime.loop.widget, urwid.Overlay) and Runtime.loop.widget.contents[1][0] == self:
            Runtime.loop.widget = Runtime.loop.widget.contents[0][0]

    @abstractmethod
    def call(self, key: str, is_cancel: bool):
        """
        The method executed if either a key has been hit or a Button has been pressed

        Don't use this to construct a Button
        Parameters
        ----------
        key : str
            The key which has been pressed or the corresponding code
        is_cancel : bool
            If the pressed key is part of the self.cancel list

        Returns
        -------
        None
        """
        self.destroy()

    def call_button(self, *args):
        """
        Method used to instance Buttons

        Parameters
        ----------
        args : tuple of any and str
            The Button object and the return code from a Button

        Returns
        -------
        None
        """
        self.call(args[1], args[1] in self.cancel)

    def pressed(self, key) -> Optional[bool]:
        """
        Keypress handling

        Parameters
        ----------
        key : str
            The pressed key

        Returns
        -------
        bool, optional
            Whether the key has been handled

        Notes
        -----
        The keypress first gets passed onto the child objects before being processed by this allowing Edit Widgets
        within this and the use of letters as keywords. Calls call.
        """
        self.call(key, key in self.cancel)
        return True


class ButtonBox(QuestionBox):
    """
    ButtonBox

    Attributes
    ----------
    NORMAL : str
        The palette name of a normal Text part
    SUBTITLE : str
        The palette name of a subtitle Text part

    Methods
    -------
    __init__
    """
    NORMAL = 'buttonbox'
    SUBTITLE = 'question_subtitle'

    def __init__(self, title: str, answers: QuestionAnswers, command: Callable, subtitle: str = None,
                 entry_id: str = None, allow_cancel: bool = True):
        """
        A box containing several buttons

        The dialogue get's displayed as soon as the constructor finished his part.
        This Object won't touch entry_id, it's just a payload given to command.

        Parameters
        ----------
        title : str
            The title of the dialogue
        answers : QuestionAnswers
            Iterable of possible answers
        command : callable
            The command which will get confronted with the results
        subtitle : str
            First non selectable entry of the List
        entry_id : str
            Untouched text field as payload
        allow_cancel : bool
            Whether a Cancel button is added and esc works

        Returns
        -------
        ButtonBox

        See Also
        --------
        QuestionBox : Basic Widget

        Notes
        -----
        The answers should be in a format like (('Question1', 'q'), ('Question2', 'u')).
        """
        self.keywords = [i[1] for i in answers if len(i[1]) == 1]
        if any(i for i in self.keywords if self.keywords.count(i) > 1):
            raise RuntimeError(f'Got keywords twice in {self.keywords} from {title}')
        cancel = ''
        potential_cancel = [i for i in 'cancel' if i not in self.keywords]
        if len(potential_cancel) > 0:
            answers = (*answers, ('Cancel', potential_cancel[0]))
            cancel = potential_cancel[0]
        answer_list = urwid.ListBox(urwid.SimpleFocusListWalker(
            ([urwid.Text((self.SUBTITLE, subtitle))] if subtitle else [])
            + [PrettyButton(i[0], on_press=self.call_button, user_data=i[1], markup=self.NORMAL,
                            highlighting=True) for i in answers]
        ))
        self.command = command
        self.entry_id = entry_id
        super().__init__(answer_list, title, allow_cancel=allow_cancel, cancel=cancel, keywords=self.keywords,
                         wrap_attr=self.NORMAL)
        self.register()

    def call(self, key: str, is_cancel: bool):
        """
        The method executed if either a key has been hit or a Button has been pressed

        Parameters
        ----------
        key : str
            The key which has been pressed or the corresponding code
        is_cancel : bool
            If the pressed key is part of the self.cancel list

        Returns
        -------
        None
        """
        super().call(key, is_cancel)
        if not is_cancel:
            self.command(**{'reply_id': key, 'title': self.title, 'entry_id': self.entry_id})


class MessageBox(QuestionBox):
    """
    MessageBox

    Attributes
    ----------
    NORMAL : str
        The palette name of a normal Text part
    OK : str
        The code used for the Ok Button
    CANCEL: str
        The code used for the Cancel Button
    command : callable
        Command to confront with the result

    Methods
    -------
    __init__
    """
    OK = 'o'
    CANCEL = 'c'
    NORMAL = 'messagebox'

    def __init__(self, title: str, subtitle: str, allow_cancel: bool = False, command: Callable = None):
        """
        Either a simple MessageBox or a confirmation dialogue

        By default this is a Message box which shows one information and allows the user to press Ok or esc.
        If allow_cancel is set to True, this is a confirmation dialogue which asks the user to press either Ok or Cancel.
        If the user entered something, the dialogue will self destruct and call command with the result.
        The dialogue will get displayed automatically after the constructor finished it's work.
        The esc key is considered as cancel.

        Parameters
        ----------
        title : str
            Title of the dialogue
        subtitle : str
            Subtitle / actual content of the dialogue
        allow_cancel : bool
            If this is a pure Message or a confirmation dialogue
        command : callable
            Command to confront with the result

        Returns
        -------
        MessageBox

        See Also
        --------
        QuestionBox : Basic Widget
        """
        text: urwid.Text = urwid.Text(subtitle)
        ok: PrettyButton = PrettyButton('Ok', on_press=self.call_button, user_data=self.OK, markup=self.NORMAL,
                                        highlighting=True)
        cancel: PrettyButton = PrettyButton('Cancel', on_press=self.call_button, user_data=self.CANCEL,
                                            markup=self.NORMAL, highlighting=True)
        columns: urwid.Columns = urwid.Columns([ok, cancel] if allow_cancel else [ok])
        box_list: urwid.ListBox = urwid.ListBox(urwid.SimpleFocusListWalker([text, columns]))
        self.command: Optional[Callable] = command
        super().__init__(box_list, title, allow_cancel=allow_cancel, cancel=self.CANCEL, keywords=(self.OK, 'esc'),
                         wrap_attr=self.NORMAL)
        self.register()

    def call(self, key: str, is_cancel: bool):
        """
        The method executed if either a key has been hit or a Button has been pressed

        Parameters
        ----------
        key : str
            The key which has been pressed or the corresponding code
        is_cancel : bool
            If the pressed key is part of the self.cancel list

        Returns
        -------
        None
        """
        self.destroy()
        if self.command:
            self.command(not is_cancel)


class CSVBox(QuestionBox):
    """
    CSVBox

    Attributes
    ----------
    NORMAL : str
        The palette name of a normal Text part
    OK : str
        The code used for the Ok Button
    CANCEL: str
        The code used for the Cancel Button
    edit : urwid.Edit
        The urwid Edit entry
    command : callable
        The command to execute in the end
    checking_command : callable
        The command to check individual entries
    existing_text : str
        The text before the CSVBox
    single_col : bool
        Whether it's running in single column mode

    Methods
    -------
    __init__
    """
    NORMAL: str = 'csvbox'
    OK: str = 'o'
    CANCEL: str = 'c'

    def __init__(self, title: str, command: Callable, checking_command: Callable, subtitle: str = None,
                 allow_cancel: bool = False, existing_text: str = None, single_col: bool = False):
        """
        Edit dialogue which accepts comma separated vectors

        It'll call command with the final results and use checking_command to test weather individual entries
        are correct or not. It'll display itself on top of everything using a overlay once constructed.
        When single_col is true it'll only return one individual item.

        Parameters
        ----------
        title : str
            Title of the dialogue
        command : callable
            the command which will get executed given the list of entries
        checking_command : callable
            A command which will validate a certain entry
        subtitle : str
            Title to place on top of the Edit field
        allow_cancel : bool
            cancelling the whole dialogue without any result is allowed
        existing_text : str
            Text which is already entered while opening the dialogue
        single_col : bool
            only one entry is allowed, the command will only receive this entry

        Returns
        -------
        CSVBox

        See Also
        --------
        QuestionBox : Basic Widget

        Examples
        --------
        >>> def cmd(arguments: List[str]):
        ...     print(arguments)
        ...
        >>> def prove(entry): return True
        ...
        >>> CSVBox('The title', cmd, prove, 'The subtitle')
        """
        self.edit: urwid.Edit = urwid.Edit(caption=f'{subtitle}\n', edit_text=existing_text or '', multiline=True,
                                           edit_pos=len(existing_text or ''))
        ok: PrettyButton = PrettyButton('Ok', on_press=self.ok, user_data=self.OK, markup=self.NORMAL,
                                        highlighting=True)
        cancel: PrettyButton = PrettyButton('Cancel', on_press=self.call, user_data=self.CANCEL,
                                            markup=self.NORMAL, highlighting=True)
        columns: urwid.Columns = urwid.Columns([ok, cancel])
        box_list: urwid.ListBox = urwid.ListBox(urwid.SimpleFocusListWalker([self.edit, columns]))
        self.command: Callable = command
        self.checking_command: Callable = checking_command
        self.existing_text: str = existing_text
        self.single_col: bool = single_col
        super().__init__(urwid.AttrWrap(box_list, self.NORMAL), title, allow_cancel=allow_cancel, cancel=self.CANCEL,
                         keywords=self.OK)
        self.register()

    def ok(self, *args):
        """
        Method executed when the Ok Button was hit

        Parameters
        ----------
        args
            Button arguments, never used

        Returns
        -------
        None
        """
        text = self.edit.get_edit_text().replace('\n', '')
        if self.existing_text == text:
            self.destroy()
            return
        argument_list: List[str] = [i.strip() for i in text.split(',')]
        if len(argument_list) != 1 and self.single_col:
            MessageBox('Invalid format', 'You are not allowed to use comas in this dialogue')
        elif all(self.checking_command(i) for i in argument_list):
            self.destroy()
            self.command(argument_list[0] if self.single_col else argument_list)
        else:
            MessageBox('Invalid format', 'Expected a comma separated vector but got an invalid one')

    def call(self, key: str, is_cancel: bool):
        """
        The method executed if either a key has been hit or the cancel Button has been pressed

        Parameters
        ----------
        key : str
            The key which has been pressed or the corresponding code
        is_cancel : bool
            If the pressed key is part of the self.cancel list

        Returns
        -------
        None
        """
        if not is_cancel:
            return self.ok()
        if self.edit.get_edit_text() != self.existing_text:
            MessageBox('Discard', 'Do you want to discard the changes', allow_cancel=True, command=self.call_msg_return)
            return
        super().call(key, is_cancel)

    def call_msg_return(self, state: bool):
        """
        The method executed by the discard dialogue
        Parameters
        ----------
        state : bool
            The answer to the discard dialogue

        Returns
        -------
        None
        """
        if state:
            self.destroy()
