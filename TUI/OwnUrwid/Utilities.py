from typing import Tuple, Union, List, Iterable

Markup = Union[str, Tuple[str, str], List[Union[str, Tuple[str, str]]]]
ColumnsOptions = Tuple[str, int, bool]
QuestionAnswer = Union[Tuple[str, str], Tuple[str, str, int], Tuple[str, str, int, str]]
QuestionAnswers = Iterable[QuestionAnswer]
