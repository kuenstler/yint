from .OwnUrwid.Utilities import QuestionAnswer

# need_to_download
watch_video: QuestionAnswer = ('Watch video', 'w', 0, 'not')
# subscribed_creator
unsubscribe_creator: QuestionAnswer = ('Unsubscribe creator', 's', 1)
subscribe_creator: QuestionAnswer = ('Subscribe creator', 's', 1, 'not')
view_in_browser: QuestionAnswer = ('View in browser', 'v')
