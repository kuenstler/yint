from threading import Lock
from webbrowser import open_new_tab

from ZODB.Connection import Connection
from transaction import TransactionManager

from DB.Channel import ChannelStorage
from DB.ChannelList import ChannelListStorage
from Utilities.Settings import Settings
from Utilities.Typings import CID
from Utilities.YoutubeFeatures import search_for_channel_by_keyword
from .OwnUrwid.GenericSearch import GenericSearch, SearchEntries
from .Runtime import Runtime
from .UsualQuestions import subscribe_creator, unsubscribe_creator, view_in_browser

lock: Lock = Lock()


class GlobalYoutubeChannelSearch(GenericSearch):
    def __init__(self):
        self._tm: TransactionManager = TransactionManager()
        self._conn: Connection = Settings.db.open(self._tm)
        super().__init__(
            'Search for Youtube channels',
            ('Videos',),
            (
                ('Do not download videos instantly', 'i', 0),
                ('Download videos instantly', 'i', 0, 'not'),
                subscribe_creator,
                unsubscribe_creator,
                view_in_browser
            )
        )

    def search_for(self, text: str) -> SearchEntries:
        """
        Search for a given text on youtube channels

        Parameters
        ----------
        text : str

        Returns
        -------
        SearchEntries
        """
        results = search_for_channel_by_keyword(text)
        with lock:
            self._conn.sync()
            channels: ChannelListStorage = self._conn.root.channels
            output: SearchEntries = []
            for i in results:
                cid: CID = CID(f'yt:channel:{i["id"]["channelId"]}')
                if cid not in channels:
                    channels[cid] = ChannelStorage.from_youtube_result(i)
                channel: ChannelStorage = channels[cid]
                output.append(((channel.name,), channel.cid, channel.description,
                               (channel.instant, *channel.button_arguments)))
            self._tm.commit()
        return output

    def answer(self, reply_id: str, title: str, entry_id: str):
        """
        The method which get's confronted with the result of the ButtonBox

        Override this if you want to implement your own behaviour
        Parameters
        ----------
        reply_id : str
            The id of the answer code
        title : str
            The title of the MessageBox
        entry_id : str
            The id of the attached entry

        Returns
        -------
        None
        """
        with lock:
            self._conn.sync()
            entry: ChannelStorage = self._conn.root.channels[entry_id]
            if reply_id == 'i':
                entry.instant = not entry.instant
            elif reply_id == subscribe_creator[1]:
                entry.subscribed = not entry.subscribed
            elif reply_id == view_in_browser[1]:
                open_new_tab(entry.url)
            self._tm.commit()

    def __del__(self):
        self._conn.close()


def global_youtube_channel_search():
    """
    Set an instance of GlobalYoutubeChannelSearch as main frame body

    Returns
    -------
    None
    """
    Runtime.frame.body = GlobalYoutubeChannelSearch()
