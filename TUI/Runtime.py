from __future__ import annotations

# .Menu
from typing import TYPE_CHECKING

import urwid
from ZODB.Connection import Connection
from transaction import TransactionManager

if TYPE_CHECKING:
    from .Menu import MainMenu
    from Utilities.Typings import Pipe


class Runtime:
    """
    Runtime

    Parameters
    ----------
    loop : urwid.MainLoop
        The main loop where everything runs in
    frame : urwid.Frame
        The main frame
    menu : MainMenu
        The Main Menu
    """
    loop: urwid.MainLoop
    frame: urwid.Frame
    menu: MainMenu
    main_pipe: Pipe
    conn: Connection
    tm: TransactionManager

    def __init__(self):
        raise RuntimeError('Tried to instanciate Runtime')
