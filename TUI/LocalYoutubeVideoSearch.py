from threading import Lock
from typing import List
from webbrowser import open_new_tab

from ZODB.Connection import Connection
from transaction import TransactionManager

from DB.Video import VideoStorage, DownloadStatus
from DB.VideoList import VideoListStorage
from Utilities.Settings import Settings
from .OwnUrwid.GenericSearch import GenericSearch, SearchEntries
from .Runtime import Runtime
from .UsualQuestions import watch_video, view_in_browser

lock: Lock = Lock()


class LocalYoutubeVideoSearch(GenericSearch):
    def __init__(self):
        """
        A local youtube video search window
        """
        self._tm: TransactionManager = TransactionManager()
        self._conn: Connection = Settings.db.open(self._tm)
        super().__init__(
            'Local youtube video search',
            ('Youtube videos',),
            (
                watch_video,
                ('Download video', 'd', 0),
                view_in_browser
            )
        )

    def search_for(self, text: str) -> SearchEntries:
        """
        Search for youtube videos in the local database

        Parameters
        ----------
        text : str
            The text to search for

        Returns
        -------
        SearchEntries
            An iterable of add_buttons

        See Also
        --------
        .GenericList.add_button : The syntax for each entry
        """
        video_list: VideoListStorage = self._conn.root.videos
        results: List[VideoStorage] = video_list.search_for(text)
        return [i.get_add_button_arguments() for i in results]

    def answer(self, reply_id: str, title: str, entry_id: str):
        """
        The method which get's confronted with the result of the ButtonBox

        Override this if you want to implement your own behaviour
        Parameters
        ----------
        reply_id : str
            The id of the answer code
        title : str
            The title of the MessageBox
        entry_id : str
            The id of the attached entry

        Returns
        -------
        None
        """
        with lock:
            self._conn.sync()
            video: VideoStorage = self._conn.root.videos[entry_id]
            if reply_id == watch_video[1]:
                pass
            elif reply_id == 'd':
                video.download_status = DownloadStatus.WAITING_FOR_DOWNLOAD_PERMISSION
            elif reply_id == view_in_browser[1]:
                open_new_tab(video.url)
            self._tm.commit()


def local_youtube_video_search():
    """
    Set an instance of LocalYoutubeVideoSearch as main frame body

    Returns
    -------
    None
    """
    Runtime.frame.body = LocalYoutubeVideoSearch()
