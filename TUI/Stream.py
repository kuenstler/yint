from .OwnUrwid.GenericList import GenericList
from .Runtime import Runtime
from .UsualQuestions import subscribe_creator, unsubscribe_creator, view_in_browser


class StreamList(GenericList):
    def __init__(self):
        """
        A list of streams

        Notes
        -----
        add_button needs 1 config argument
        (subscribed)
        """
        super().__init__(
            ('Stream List',),
            (
                ('Cancel downloading stream', 'c'),
                view_in_browser,
                unsubscribe_creator,
                subscribe_creator,
            )
        )


def stream(*args):
    """
    Initializes StreamList and overwrites the main frames body

    Parameters
    ----------
    args
        Unused

    Returns
    -------
    None
    """
    Runtime.frame.body = StreamList()
