from threading import Lock
from typing import List
from webbrowser import open_new_tab

from ZODB.Connection import Connection
from transaction import TransactionManager

from DB.Channel import ChannelStorage
from DB.ChannelList import ChannelListStorage
from Utilities.Settings import Settings
from .OwnUrwid.GenericSearch import GenericSearch, SearchEntries
from .Runtime import Runtime
from .UsualQuestions import view_in_browser, subscribe_creator, unsubscribe_creator

lock: Lock = Lock()


class LocalTwitchChannelSearch(GenericSearch):
    def __init__(self):
        """
        A local twitch channel search window
        """
        self._tm: TransactionManager = TransactionManager()
        self._conn: Connection = Settings.db.open(self._tm)
        super().__init__(
            'Local twitch channel search',
            ('Twitch channels',),
            (
                (*unsubscribe_creator[:2], 0, *unsubscribe_creator[3:]),
                (*subscribe_creator[:2], 0, *subscribe_creator[3:]),
                view_in_browser
            )
        )

    def search_for(self, text: str) -> SearchEntries:
        """
        Search for twitch channels in the local database

        Parameters
        ----------
        text : str
            The text to search for

        Returns
        -------
        SearchEntries
            An iterable of add_buttons

        See Also
        --------
        .GenericList.add_button : The syntax for each entry
        """
        channel_list: ChannelListStorage = self._conn.root.channels
        results: List[ChannelStorage] = channel_list.search_for(text, 'tw')
        return [((i.name,), i.cid, i.description, (i.subscribed,)) for i in results]

    def answer(self, reply_id: str, title: str, entry_id: str):
        """
        The method which get's confronted with the result of the ButtonBox

        Override this if you want to implement your own behaviour
        Parameters
        ----------
        reply_id : str
            The id of the answer code
        title : str
            The title of the MessageBox
        entry_id : str
            The id of the attached entry

        Returns
        -------
        None
        """
        with lock:
            self._conn.sync()
            channel: ChannelStorage = self._conn.root.channels[entry_id]
            if reply_id == view_in_browser[1]:
                open_new_tab(channel.url)
            elif reply_id == subscribe_creator[1]:
                channel.subscribed = not channel.subscribed
            self._tm.commit()


def local_twitch_channel_search():
    """
    Set an instance of LocalTwitchChannelSearch as main frame body

    Returns
    -------
    None
    """
    Runtime.frame.body = LocalTwitchChannelSearch()
