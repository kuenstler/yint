import os
from threading import Lock, Thread
from typing import Tuple, Dict
from webbrowser import open_new_tab

import urwid

from DB.DownloadList import DownloadListStorage
from DB.Video import VideoStorage, DownloadStatus, MarkWatched
from DB.VideoList import VideoListStorage
from Utilities.Settings import Settings
from Utilities.YoutubeFeatures import like_video
from .OwnUrwid.GenericList import GenericList
from .OwnUrwid.TabMixin import TabPileMixin, TabColumnsMixin
from .Runtime import Runtime
from .UsualQuestions import watch_video, unsubscribe_creator, subscribe_creator, view_in_browser

lock: Lock = Lock()


class DownLoadList(GenericList):
    def __init__(self):
        """
        A list of potential downloads

        Notes
        -----
        A add_button function needs 2 configuration options which are
        (need_to_download, subscribed_creator)
        """
        super().__init__(
            ('Download List',),
            (
                ('Download video while marking watched', 'd', 0),
                ('Download video without marking watched', 'm', 0),
                ('Do not download video', 'n', 0),
                ('Download video with prioritisation', 'p', 0),
                watch_video,
                view_in_browser,
                unsubscribe_creator,
                subscribe_creator,
            ),
            right=True
        )

    def answer(self, reply_id: str, title: str, entry_id: str):
        with lock:
            Runtime.conn.sync()
            entry: VideoStorage = Runtime.conn.root.videos[entry_id]
            downloads: DownloadListStorage = Runtime.conn.root.download_list
            if reply_id == 'd':
                entry.download_status = DownloadStatus.NOT_DOWNLOADED
                entry.mark_watched = MarkWatched.MARK_AS_WATCHED
                downloads.add_video(entry.vid)
            elif reply_id == 'm':
                entry.download_status = DownloadStatus.NOT_DOWNLOADED
                entry.mark_watched = MarkWatched.NO_NOT_MARK_AS_WATCHED
                downloads.add_video(entry.vid)
            elif reply_id == 'n':
                entry.download_status = DownloadStatus.NEVER_GOING_TO_GET_DOWNLOADED
                entry.mark_watched = MarkWatched.NO_NOT_MARK_AS_WATCHED
            elif reply_id == 'p':
                entry.download_status = DownloadStatus.NOT_DOWNLOADED
                entry.mark_watched = MarkWatched.MARK_AS_WATCHED
                downloads.add_video(entry.vid, position=0)
            elif reply_id == subscribe_creator[1]:
                entry.channel.subscribed = not entry.channel.subscribed
            elif reply_id == view_in_browser[1]:
                open_new_tab(entry.url)
            Runtime.tm.commit()
        if reply_id in ('d', 'm', 'p'):
            os.write(Runtime.main_pipe, f'add_pending\x1f{entry_id}\n'.encode())
        if reply_id in ('d', 'm', 'n', 'p'):
            self.remove_button(entry_id)


class LikeList(GenericList):
    like_dict: Dict[int, str] = {1: 'l', 2: '2', 3: '3', 4: '4', 5: '5', -1: 'd', -2: 'i', -3: 'k', -4: 't', -5: 'm',
                                 0: 'n'}
    like_backward: Dict[str, int] = {j: i for i, j in like_dict.items()}

    def __init__(self):
        """
        A list of potential likes

        Notes
        -----
        A add_button function needs 2 configuration options which are
        (need_to_download, subscribed_creator)
        """
        super().__init__(
            ('Like list',),
            (
                ('Like video once', self.like_dict[1]),
                ('Like video 2 times', self.like_dict[2], 2),
                ('Like video 3 times', self.like_dict[3], 3),
                ('Like video 4 times', self.like_dict[4], 4),
                ('Like video 5 times', self.like_dict[5], 5),
                ('Do not like or dislike the video', self.like_dict[0]),
                ('Dislike video', self.like_dict[-1]),
                ('Dislike twice', self.like_dict[-2], 2),
                ('Dislike 3 times', self.like_dict[-3], 3),
                ('Dislike 4 times', self.like_dict[-4], 4),
                ('Dislike 5 times', self.like_dict[-5], 5),
                watch_video,
                view_in_browser,
                unsubscribe_creator,
                subscribe_creator,
            ),
            left=True
        )

    def answer(self, reply_id: str, title: str, entry_id: str):
        with lock:
            Runtime.conn.sync()
            entry: VideoStorage = Runtime.conn.root.videos[entry_id]
            if reply_id in self.like_backward.keys():
                entry.like = self.like_backward[reply_id]
            elif reply_id == subscribe_creator[1]:
                entry.channel.subscribed = not entry.channel.subscribed
            elif reply_id == view_in_browser[1]:
                open_new_tab(entry.url)
            Runtime.tm.commit()
        if reply_id in self.like_backward.keys():
            Thread(target=like_video, args=(entry.base_id, entry.like))
            self.remove_button(entry_id)


class DownloadLikeList(TabColumnsMixin, urwid.Columns):
    def __init__(self):
        """
        A combined list of potential downloads and likes
        """
        self.like: LikeList = LikeList()
        self.download: DownLoadList = DownLoadList()
        super().__init__([self.download, self.like])

    def add_button(self, *args, **kwargs):
        self.like.add_button(*args, **kwargs)
        self.download.add_button(*args, **kwargs)


class PendingList(GenericList):
    def __init__(self):
        """
        A list of pending downloads
        """
        super().__init__(
            ('Pending downloads',),
            (
                ('Move up', 'u', 0),
                ('Move to top', 't', 0),
                ('Move down', 'd', 1),
                ('Move to bottom', 'b', 1),
                ('Cancel download', 'c')
            )
        )

    def add_button(self, texts: Tuple[str, ...], id: str, subtitle: str = '', configuration: Tuple[bool, ...] = ()):
        """
        Add a Button to this ListBox

        If no configuration is present, it will automatically get filled out

        Parameters
        ----------
        texts : tuple of str
            The text to display
        id : str
            The id attached to the text
        configuration :  tuple of bool
            The configuration of optional questions
        subtitle : str
            The subtitle of the video

        Returns
        -------
        None
        """
        if not configuration:
            configuration = (len(self.configuration), False)
        if len(self.configuration) > 0:
            entry = self.list[-1][0].user_data
            self.configuration[entry] = (self.configuration[entry][0], True)
        super().add_button(texts, id, subtitle, configuration)

    def remove_button(self, id: str):
        """
        Remove a button with a given id from the list

        Parameters
        ----------
        id : str
            The id of the button

        Returns
        -------
        None
        """
        super(PendingList, self).remove_button(id)
        if self.list:
            entry = self.list[0][0].user_data
            self.configuration[entry] = (False, self.configuration[entry][1])

    def move_button_up(self, vid: str, top: bool = False):
        """
        Move a button one entry up or to the top

        Parameters
        ----------
        vid : str
            The id of the video to move
        top : bool
            Whether or not to move the entry to the top of the list

        Returns
        -------
        None

        Warnings
        --------
        Must be executed within a lock, changes the database
        """
        download_list: DownloadListStorage = Runtime.conn.root.download_list
        download_list.move_up(vid, top=top)
        if vid in self.configuration and vid in self.subtitles:
            index: int = [i for i, j in enumerate(self.list) if j.contents[0][0].user_data == vid][0]
            if index <= 0:
                return
            if top:
                entry = self.list[index]
                del self.list[index]
                self.list.insert(0, entry)
            else:
                self.list[index], self.list[index - 1] = self.list[index - 1], self.list[index]
            second_entry: str = self.list[index].contents[0][0].user_data
            last: bool = index != len(self.list) - 1
            self.configuration[second_entry] = (True, last)
            self.configuration[vid] = (not (top or index == 1), True)

    def move_button_down(self, vid: str, bottom: bool = False):
        """
        Move a button one entry down or to the bottom

        Parameters
        ----------
        vid : str
            The id of the video to move
        bottom : bool
            Whether or not to move the entry to the bottom of the list

        Returns
        -------
        None

        Warnings
        --------
        Must be executed within a lock, changes the database
        """
        download_list: DownloadListStorage = Runtime.conn.root.download_list
        download_list.move_down(vid, bottom=bottom)
        if vid in self.configuration and vid in self.subtitles:
            last_item: int = len(self.list) - 1
            index: int = [i for i, j in enumerate(self.list) if j.contents[0][0].user_data == vid][0]
            if index >= last_item:
                return
            if bottom:
                self.list.append(self.list[index])
                del self.list[index]
            else:
                self.list[index], self.list[index + 1] = self.list[index + 1], self.list[index]
            second_entry: str = self.list[index].contents[0][0].user_data
            first: bool = index != 0
            self.configuration[second_entry] = (first, True)
            self.configuration[vid] = (True, not (bottom or index == last_item - 1))

    def answer(self, reply_id: str, title: str, entry_id: str):
        """
        The method which get's confronted with the result of the ButtonBox

        Override this if you want to implement your own behaviour
        Parameters
        ----------
        reply_id : str
            The id of the answer code
        title : str
            The title of the MessageBox
        entry_id : str
            The id of the attached entry

        Returns
        -------
        None
        """
        with lock:
            Runtime.conn.sync()
            download_list: DownloadListStorage = Runtime.conn.root.download_list
            entry: VideoStorage = Runtime.conn.root.videos[entry_id]
            if reply_id == 'u':
                self.move_button_up(entry_id)
            elif reply_id == 't':
                self.move_button_up(entry_id, top=True)
            elif reply_id == 'd':
                self.move_button_down(entry_id)
            elif reply_id == 'b':
                self.move_button_down(entry_id, bottom=True)
            elif reply_id == 'c':
                download_list.remove_video(entry_id)
                entry.download_status = DownloadStatus.WAITING_FOR_DOWNLOAD_PERMISSION
                self.remove_button(entry_id)
                os.write(Runtime.main_pipe, f'add_dwnload\x1f{entry_id}\n'.encode())
            Runtime.tm.commit()


class DownloadLikePendingList(TabPileMixin, urwid.Pile):
    def __init__(self):
        """
        A combined list of potential downloads, pending downloads and likes
        """
        self.pending: PendingList = PendingList()
        self.download_like: DownloadLikeList = DownloadLikeList()
        self.like: LikeList = self.download_like.like
        self.download: DownLoadList = self.download_like.download
        super().__init__((('weight', 2, self.download_like), self.pending))


def video(*args):
    """
    Initializes DownloadLikeList and overwrites the main frames body

    Parameters
    ----------
    args
        Unused

    Returns
    -------
    None
    """
    Runtime.frame.body = DownloadLikePendingList()
    with Settings.db.transaction() as t:
        videos: VideoListStorage = t.root.videos
        for i in videos.download_items:
            Runtime.frame.body.download.add_button(*i)
        for i in videos.like_items:
            Runtime.frame.body.like.add_button(*i)
        for i in t.root.download_list:
            vid: VideoStorage = t.root.videos[i]
            Runtime.frame.body.pending.add_button((vid.lurid_title,), vid.vid, vid.readable_description)
