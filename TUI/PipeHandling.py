from typing import List

import urwid
from ZODB.Connection import Connection

from DB.Video import VideoStorage
from Utilities.Settings import Settings
from .Runtime import Runtime
from .Video import DownloadLikePendingList


def handle_pipe(data: bytes):
    """
    Handle data put into a pipe

    Parameters
    ----------
    data : bytes
        The information

    Returns
    -------
    None

    Notes
    -----
    The separator \x1f is the ascii character for Unit Separator
    The data should start with one of the following strings to indicate an action:
        prog\x1fNAME\x1fPROGRESS\n
            Set the progress bar at the footer to a given text and progress
        add_dwnload\x1fID\n
            Add a video to download_list
        add_like\x1fID\n
            Add a video to like_list
        rm_pending\x1fID\n
            Remove a video from pending list
        add_pending\x1fID\n
            Add a video to pending list
        txt\x1fTEXT\n
            Write free text

    Use it like ```os.write(Runtime.main_pipe, 'prog:Title:0\n'.encode())```
    """
    string: str = data.decode()
    conn: Connection = Settings.db.open()
    for i in string.split('\n'):
        body = Runtime.frame.body
        footer = Runtime.frame.footer
        str_list: List[str] = i.split('\x1f')
        if str_list[0] == '':
            pass
        elif str_list[0] == 'prog':
            if not isinstance(Runtime.frame.footer, urwid.Pile):
                Runtime.frame.footer = urwid.Pile([
                    urwid.Padding(urwid.Text(str_list[1]), align='center'),
                    urwid.ProgressBar('progress_normal', 'progress_completed', current=int(float(str_list[2]) * 10),
                                      done=1000, satt='progress_smooth')
                ])
            else:
                if footer.contents[0][0].base_widget.text != str_list[1]:
                    footer.contents[0][0].base_widget.set_text(str_list[1])
                footer.contents[1][0].current = int(float(str_list[2]) * 10)
        elif str_list[0] == 'add_dwnload':
            entry: VideoStorage = conn.root.videos[str_list[1]]
            body = Runtime.frame.body
            if isinstance(body, DownloadLikePendingList):
                body.download.add_button(*entry.get_add_button_arguments())
        elif str_list[0] == 'add_like':
            entry: VideoStorage = conn.root.videos[str_list[1]]
            body = Runtime.frame.body
            if isinstance(body, DownloadLikePendingList):
                body.like.add_button(*entry.get_add_button_arguments(like=True))
        elif str_list[0] == 'rm_pending':
            if isinstance(body, DownloadLikePendingList):
                body.pending.remove_button(str_list[1])
        elif str_list[0] == 'add_pending':
            if isinstance(body, DownloadLikePendingList):
                entry: VideoStorage = conn.root.videos[str_list[1]]
                body.pending.add_button((entry.lurid_title,), entry.vid, subtitle=entry.readable_description)
        elif str_list[0] == 'txt':
            footer = Runtime.frame.footer
            if isinstance(footer, urwid.Text):
                footer.set_text(str_list[1])
            else:
                Runtime.frame.footer = urwid.Text(str_list[1])
        else:
            raise RuntimeError(f'Unexpected string "{string}" in Pipe')
    conn.close()
