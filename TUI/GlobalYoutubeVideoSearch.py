from threading import Lock
from typing import List
from webbrowser import open_new_tab

from ZODB.Connection import Connection
from transaction import TransactionManager

from DB.Channel import ChannelStorage
from DB.ChannelList import ChannelListStorage
from DB.Stream import StreamStorage
from DB.StreamList import StreamListStorage
from DB.Video import VideoStorage, DownloadStatus
from DB.VideoList import VideoListStorage
from Utilities.Settings import Settings
from Utilities.Typings import CID, VID, SID
from Utilities.YoutubeFeatures import search_for_video_by_keyword, search_for_video_by_id, search_for_channel_by_id, \
    NormalVideoResult
from .OwnUrwid.GenericSearch import GenericSearch, SearchEntries
from .Runtime import Runtime
from .UsualQuestions import view_in_browser, subscribe_creator, unsubscribe_creator

lock: Lock = Lock()


class GlobalYoutubeVideoSearch(GenericSearch):
    def __init__(self):
        self._tm: TransactionManager = TransactionManager()
        self._conn: Connection = Settings.db.open(self._tm)
        super().__init__(
            'Search for Youtube videos',
            ('Videos',),
            (
                ('Download/like video', 'd'),
                view_in_browser,
                subscribe_creator,
                unsubscribe_creator
            )
        )

    def check_channel(self, search_result: NormalVideoResult) -> ChannelStorage:
        cid: CID = ChannelStorage.get_cid_from_yt_id(search_result["snippet"]["channelId"])
        channels: ChannelListStorage = self._conn.root.channels
        if cid not in channels:
            channels[cid] = ChannelStorage.from_youtube_result(search_for_channel_by_id(cid))
        return channels[cid]

    def search_for(self, text: str) -> SearchEntries:
        """
        Search for youtube videos, if a id pattern has been found, instead show only this

        Parameters
        ----------
        text : url or search string

        Returns
        -------
        SearchEntries
        """
        if 'youtube.com/watch?v=' in text or 'youtu.be' in text:
            # Handle urls as given ids
            text = text.split('youtube.com/watch?v=')[-1]
            text = text.split('youtu.be/')[-1]
            text = text.split('?')[0]  # remove things like ?feature=share
            text = text.split(';')[0]  # remove things like ?v=vid;feature=share
            results: List[NormalVideoResult] = [search_for_video_by_id(text)]
        else:
            results: List[NormalVideoResult] = search_for_video_by_keyword(text)
        with lock:
            self._conn.sync()
            videos: VideoListStorage = self._conn.root.videos
            streams: StreamListStorage = self._conn.root.streams
            output: SearchEntries = []
            for i in results:
                channel: ChannelStorage = self.check_channel(i)
                if i['snippet']['liveBroadcastContent'] == 'none':
                    vid: VID = VideoStorage.get_vid_from_yt_id(i['id'])
                    if vid not in videos.keys():
                        videos[vid] = VideoStorage.from_youtube_search_result(i, channel, no_like=True)
                    video: VideoStorage = videos[vid]
                    output.append(video.get_add_button_arguments())
                else:
                    sid: SID = StreamStorage.get_sid_from_yt_id(i['id'])
                    if sid not in streams:
                        streams[sid] = StreamStorage.from_youtube_result(i, channel, no_like=True)
                    stream: StreamStorage = streams[sid]
                    output.append(((stream.readable_name,), stream.sid, stream.description or '',
                                   stream.button_arguments))
            self._tm.commit()
        return output

    def answer(self, reply_id: str, title: str, entry_id: str):
        """
        The method which get's confronted with the result of the ButtonBox

        Override this if you want to implement your own behaviour
        Parameters
        ----------
        reply_id : str
            The id of the answer code
        title : str
            The title of the MessageBox
        entry_id : str
            The id of the attached entry

        Returns
        -------
        None
        """
        with lock:
            self._conn.sync()
            entry: VideoStorage = self._conn.root.videos[entry_id]
            if reply_id == 'd':
                entry.download_status = DownloadStatus.WAITING_FOR_DOWNLOAD_PERMISSION
                entry.like = None
            elif reply_id == subscribe_creator[1]:
                entry.channel.subscribed = not entry.channel.subscribed
            elif reply_id == view_in_browser[1]:
                open_new_tab(entry.url)
            self._tm.commit()

    def __del__(self):
        self._conn.close()


def global_youtube_video_search():
    """
    Set an instance of GlobalYoutubeVideoSearch as main frame body

    Returns
    -------
    None
    """
    Runtime.frame.body = GlobalYoutubeVideoSearch()
