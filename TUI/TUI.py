import urwid
from transaction import TransactionManager

from Utilities.Settings import Settings
from .GlobalYoutubeChannelSearch import global_youtube_channel_search
from .GlobalYoutubeVideoSearch import global_youtube_video_search
from .LocalTwitchChannelSearch import local_twitch_channel_search
from .LocalYoutubeChannelSearch import local_youtube_channel_search
from .LocalYoutubeVideoSearch import local_youtube_video_search
from .Menu import MainMenu
from .OwnUrwid.NavigateableFrame import NavigateableFrame
from .PipeHandling import handle_pipe
from .Runtime import Runtime
from .SettingsWindow import settings
from .Stream import stream
from .Subscriptions import subscriptions
from .Video import video


def no_action(*args):
    pass


menu: MainMenu = MainMenu((
    ('Video', video),
    ('Stream', stream),
    ('Local Search', (
        ('Youtube Video', local_youtube_video_search),
        ('Youtube Channel', local_youtube_channel_search),
        ('Twitch Channel', local_twitch_channel_search)
    )),
    ('Global Search', (
        ('Youtube Video', global_youtube_video_search),
        ('Youtube Channel', global_youtube_channel_search),
        ('Twitch Channel', no_action),
        ('Twitch Video', no_action)
    )),
    ('Subscriptions', subscriptions),
    ('Settings', settings)
))
Runtime.menu = menu

palette = [
    (None, 'white', 'black', '', 'g100', 'g0'),
    ('sub_menu_button', 'light gray', 'black', '', 'g50', 'g10'),
    ('sub_menu_button_focus', 'white', 'dark blue', '', 'g90', '#006'),
    ('main_menu_button', 'light gray', 'black', '', 'g50', 'g0'),
    ('main_menu_button_focus', 'white', 'dark blue', '', 'g90', '#004'),
    ('buttonbox', 'white', 'black', '', 'g100', 'g0'),
    ('buttonbox_highlight', 'white,underline', 'black', 'underline', 'g100,underline', 'g0'),
    ('csvbox', 'buttonbox'),
    ('messagebox', 'buttonbox'),
    ('messagebox_highlight', 'buttonbox_highlight'),
    ('csvbox_highlight', 'buttonbox_highlight'),
    ('progress_normal', 'white', 'light gray', '', 'g100', 'g20'),
    ('progress_completed', 'black', 'white', '', 'g0', 'g80'),
    ('progress_smooth', 'progress_normal'),
]


def run():
    Runtime.tm = TransactionManager()
    Runtime.conn = Settings.db.open(Runtime.tm)
    Runtime.frame = NavigateableFrame(
        urwid.Filler(urwid.Text('Nothing here')),
        menu,
        urwid.Text('Still nothing here'),
        focus_part='header'
    )
    Runtime.frame.base_class = True
    Runtime.loop = urwid.MainLoop(Runtime.frame, palette)
    Runtime.loop.screen.set_terminal_properties(colors=256)
    Runtime.main_pipe = Runtime.loop.watch_pipe(handle_pipe)
    video()
    Runtime.loop.run()


if __name__ == '__main__':
    run()
