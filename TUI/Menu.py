from __future__ import annotations

from typing import Tuple, Callable, Union, List, Iterable

import urwid

from .OwnUrwid.PrettyButton import FocusPrettyButton
from .Runtime import Runtime


class MenuButton(FocusPrettyButton):
    def __init__(self, caption: str, callback: Callable, name: str, context: str = None):
        """
        General menu button, just a wrapper for FocusPrettyButton

        Parameters
        ----------
        caption : str
            The caption of the Button
        callback : callable
            The function to execute if pressed
        name : str
            The palette name of the Button
        context : str
            a free text field attached to the Button
        """
        super(MenuButton, self).__init__((name, caption), on_press=callback, user_data=context,
                                         markup=name, left='', right='')


class SubMenuButton(MenuButton):
    """
    SubMenuButton

    Parameters
    ----------
    parent : MainMenuButton
        The parent MainMenuButton
    caption : str
        The caption of the Button
    callback : callable
        The function to execute if pressed

    Methods
    -------
    __init__
    """

    def __init__(self, parent: MainMenuButton, caption: str, callback: Callable):
        """
        A Button from a SubMenu

        Parameters
        ----------
        parent : MainMenuButton
            The parent MainMenuButton
        caption : str
            The caption of the Button
        callback : callable
            The function to execute if pressed
        """
        self.parent: MainMenuButton = parent
        self.caption: str = caption
        self.callback: Callable = callback
        super(SubMenuButton, self).__init__(caption, self.call, 'sub_menu_button')

    def call(self, *args):
        """
        The function which does all the fancy stuff

        Parameters
        ----------
        args : tuple of any and str
            The Button object and the return code from a Button

        Returns
        -------
        None
        """
        self.parent.collapse()
        Runtime.frame.body = urwid.Filler(urwid.Text(f'You chose {self.caption} from {self.parent.caption}'))
        self.callback()
        if Runtime.frame.body.selectable():
            Runtime.frame.focus_part = 'body'


SubMenuConfig = Tuple[Tuple[str, Callable]]
MainMenuButtonConfig = Union[SubMenuConfig, Callable]


class MainMenuButton(urwid.Pile):
    """
    MainMenuButton

    Parameters
    ----------
    callback: callable or list of SubMenuButton
        The callback, either a command or a list of SubMenuButtons
    caption : str
        Caption of this Button

    Methods
    -------
    __init__
    collapse
    """

    def __init__(self, caption: str, config: MainMenuButtonConfig):
        """
        A Button which either accepts a callback as config or a SubMenuConfig

        Parameters
        ----------
        caption : str
            Caption of this Button
        config : MainMenuButtonConfig
            Either a callback or a SubMenuConfig
        """
        self.callback: Union[Callable, List[SubMenuButton]]
        if isinstance(config, tuple):
            self.callback = [(SubMenuButton(self, *i), self.options()) for i in config]
        else:
            self.callback = config
        self.caption: str = caption
        button = MenuButton(self.caption, self.call, 'main_menu_button')
        super(MainMenuButton, self).__init__([button])

    def call(self, *args):
        """
        The actual magic

        Parameters
        ----------
        args : tuple of any and str
            The Button object and the return code from a Button

        Returns
        -------
        None
        """
        for i in Runtime.menu.contents:
            i[0].collapse()
        if isinstance(self.callback, list):
            self.collapse()
            self.contents += self.callback
            self.focus_position = 1
        else:
            self.collapse()
            Runtime.frame.body = urwid.Filler(urwid.Text(f'You chose {self.caption}'))
            self.callback()
            if Runtime.frame.body.selectable():
                Runtime.frame.focus_part = 'body'

    def collapse(self, *args):
        """
        Collapse the SubMenu if existent

        Parameters
        ----------
        args : tuple of any and str
            The Button object and the return code from a Button

        Returns
        -------
        None
        """
        del self.contents[1:]


MainMenuConfig = Iterable[Tuple[str, MainMenuButtonConfig]]


class MainMenu(urwid.Columns):
    def __init__(self, items: MainMenuConfig):
        """
        A MainMenu

        Parameters
        ----------
        items : MainMenuConfig
            config to generate the MenuButtons and SubMenuButtons
        """
        elements: List[MainMenuButton] = [MainMenuButton(*i) for i in items]
        super(MainMenu, self).__init__(elements)
