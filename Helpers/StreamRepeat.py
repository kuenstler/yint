import os
from datetime import datetime, timedelta, timezone
from functools import partial
from pathlib import Path
from subprocess import Popen, PIPE
from threading import Thread, Lock
from time import sleep
from typing import List

from ZODB.Connection import Connection
from transaction import TransactionManager

from DB.Stream import StreamStorage
from DB.StreamList import StreamListStorage
from Utilities.Settings import Settings
from Utilities.Typings import SID

lock: Lock = Lock()


class StreamSync(Thread):
    def __init__(self, sid: SID):
        self._tm: TransactionManager = TransactionManager()
        self._conn: Connection = Settings.db.open(self._tm)
        self._id: SID = sid
        super().__init__(name=f'StreamSync({sid})')

    @staticmethod
    def handle_several_files(base_file: Path, target_file: Path) -> bool:
        """
        If several files are present, merge them. If only one file is present, move it to original_file

        Parameters
        ----------
        base_file : Path
            The base without any extension
        target_file : Path
            The file you want to write or wrote already

        Returns
        -------
        bool
            Whether or not something has been done, False if target_file didn't exist
        """
        original_file: Path = base_file.with_suffix('.mkv')
        if original_file.exists() and target_file.exists():
            tmp_file: Path = base_file.with_suffix('.tmp.mkv')
            playlist: Path = base_file.with_suffix('.txt')
            with playlist.open('w') as f:
                f.write(f'file {original_file}\n')
                f.write(f'file {target_file}\n')
            ffmpeg: Popen = Popen([
                'ffmpeg', '-y', '-f', 'concat', '-safe', '0', '-i', str(playlist), '-c', 'copy', str(tmp_file)
            ], stdout=PIPE, stderr=PIPE)
            if ffmpeg.wait():
                raise RuntimeError(ffmpeg.communicate()[1].decode())
            os.remove(str(playlist))
            os.remove(str(target_file))
            tmp_file.replace(original_file)
        elif target_file.exists():
            target_file.rename(original_file)
        else:
            return False
        return True

    def run(self) -> None:
        stream: StreamStorage = self._conn.root.streams[self._id]
        while not Settings.quitting and stream.start_time > datetime.now(timezone.utc):
            sleep(2)
        if Settings.quitting:
            del self
            return
        base_file: Path = Settings.general.storage_location / f'{stream.channel.name}_{stream.start_time.isoformat()}'
        target_file: Path = base_file.with_suffix('.current.mkv')
        self.handle_several_files(base_file, target_file)
        streamlink: Popen = Popen([
            'streamlink', '--force-progress',
            'https://' + (f'youtube.com/watch?v={stream.base_id}' if stream.sid.startswith('yt')
                          else f'twitch.tv/{stream.channel.base_id}'),
            'best', '-o', str(target_file)
        ], stdout=PIPE, stderr=PIPE)
        current_line: str = ''
        for character in iter(partial(streamlink.stderr.read, 1), b''):
            # streamlink outputs the current progress to stderr and uses \r
            if Settings.quitting:
                streamlink.kill()
                break
            current_line += character.decode()
            if current_line[-2:] not in ('\n', '\r'):
                continue
            # todo: add logic for a display
            # should be a line like '[download][file.mkv] Written 7.6 MB (6s @ 1.2 MB/s)'
            current_line = ''
        if self.handle_several_files(base_file, target_file):
            if not streamlink.poll():
                with lock:
                    self._conn.sync()
                    stream.ended = True
                    self._tm.commit()
        del self

    def __del__(self):
        self._conn.close()


class StreamRepeat(Thread):
    def __init__(self):
        self._tm: TransactionManager = TransactionManager()
        self._conn: Connection = Settings.db.open(self._tm)
        self._running: List[str] = []
        super().__init__(name='StreamRepeat')
        self.daemon = True

    def run(self) -> None:
        while not Settings.quitting:
            self._conn.sync()
            streams: StreamListStorage = self._conn.root.streams
            active_streams: List[StreamStorage] = [i for i in streams.values() if not i.ended]
            for stream in active_streams:
                if stream.start_time < datetime.now(timezone.utc) - timedelta(days=1):
                    # if start_time is more than 2 days away, mark it as ended
                    with lock:
                        self._conn.sync()
                        stream.ended = True
                        self._tm.commit()
                elif stream.sid not in self._running:
                    self._running.append(stream.sid)
                    with lock:
                        Settings.threads.append(StreamSync(stream.sid))
                        Settings.threads[-1].start()
            sleep(5)

    def __del__(self):
        self._conn.close()
