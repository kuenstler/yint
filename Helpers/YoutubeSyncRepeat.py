from datetime import datetime
from threading import Thread, Lock
from time import sleep
from typing import List

from ZODB.Connection import Connection
from feedparser import parse, FeedParserDict
from transaction import TransactionManager

from DB.Channel import ChannelStorage
from DB.ChannelList import ChannelListStorage
from DB.DownloadList import DownloadListStorage
from DB.Stream import StreamStorage
from DB.StreamList import StreamListStorage
from DB.Video import VideoStorage
from DB.VideoList import VideoListStorage
from Utilities.Settings import Settings, Youtube
from Utilities.Typings import VID, SID
from Utilities.YoutubeFeatures import search_for_video_by_id_minimal, MinimalVideoResult

lock: Lock = Lock()


class ChannelSyncRepeat(Thread):
    def __init__(self):
        self._tm: TransactionManager = TransactionManager()
        self._connection: Connection = Settings.db.open(self._tm)
        super().__init__(name='YoutubeSyncRepeat')
        self.daemon = True

    def run(self) -> None:
        while not Settings.quitting:
            if Settings.db_channel_dirty_bit:
                with lock:
                    Settings.db_channel_dirty_bit = False
                self._connection.sync()
            channel_list: ChannelListStorage = self._connection.root.channels
            channels: List[ChannelStorage] = channel_list.sync_channels
            videos: VideoListStorage = self._connection.root.videos
            streams: StreamListStorage = self._connection.root.streams
            downloads: DownloadListStorage = self._connection.root.download_list
            for channel in channels:
                url: str = f'https://www.youtube.com/feeds/videos.xml?channel_id={channel.cid.split(":")[2]}'
                while True:
                    try:
                        feed: FeedParserDict = parse(url)
                        feed = feed['entries']
                        break
                    except KeyError:
                        # {'feed': {}, 'entries': [], 'bozo': 1, 'bozo_exception':
                        # URLError(gaierror(-2, 'Name or service not known'))}
                        sleep(10)
                for entry in feed:
                    vid: VID = VID(entry['id'])
                    rating: float = float(entry['media_starrating']['average'])
                    rating_count: int = int(entry['media_starrating']['count'])
                    views: int = int(entry['media_statistics']['views'])
                    if vid not in videos and vid not in streams:
                        response: MinimalVideoResult = search_for_video_by_id_minimal(vid.split(':')[2])
                        if vid not in videos and response['snippet']['liveBroadcastContent'] == 'none':
                            video: VideoStorage = VideoStorage(
                                vid,
                                entry['title'],
                                '\n'.join(j.strip() for j in entry['summary'].split('\n')),
                                channel,
                                datetime.fromisoformat(entry['published'].replace('Z', '+00:00')),
                                float(rating),
                                int(rating_count),
                                views,
                                (response['snippet']['thumbnails']['maxres']['url']
                                 if 'maxres' in response['snippet']['thumbnails'].keys()
                                 else entry['media_thumbnail'][0]['url'])
                            )
                            with lock:
                                self._connection.sync()
                                videos[vid] = video
                                Settings.db_channel_dirty_bit = False
                                if channel.instant:
                                    downloads.add_video(vid)
                                self._tm.commit()
                            video.register_lists()
                        else:
                            sid: SID = SID(vid.replace('video', 'stream'))
                            stream: StreamStorage = StreamStorage(
                                sid,
                                datetime.fromisoformat(response['liveStreamingDetails']['scheduledStartTime'].replace(
                                    'Z', '+00:00'
                                )),
                                entry['title'],
                                channel
                            )
                            with lock:
                                self._connection.sync()
                                streams[sid] = stream
                                Settings.db_channel_dirty_bit = False
                                self._tm.commit()
                    elif Youtube.update_entries and vid in videos:
                        with lock:
                            self._connection.sync()
                            videos[vid].rating = rating
                            videos[vid].rating_count = rating_count
                            videos[vid].views = views
                            Settings.db_channel_dirty_bit = False
                            self._tm.commit()
            sleep(Youtube.timeout)

    def __del__(self):
        self._connection.close()
