from datetime import datetime
from threading import Thread
from time import sleep

from DB.Ping import PingStorage
from Utilities.InetUtils import ping, traceroute
from Utilities.Settings import Settings


class SinglePing(Thread):
    def __init__(self, destination: str):
        self.time: datetime = datetime.now()
        self.destination: str = destination
        super(SinglePing, self).__init__(name='SinglePing')
        self.daemon = True

    def run(self) -> None:
        error: bool = False
        error_code: str = ''
        duration: float = 0
        try:
            duration = ping(self.destination)[0]
        except ConnectionError as err:
            error = True
            error_code = err.args[1]
        finally:
            with Settings.db.transaction() as t:
                t.root.pings[int(self.time.timestamp())] = PingStorage(
                    duration, self.time, error=error, error_code=error_code
                )


class PingRepeat(Thread):
    def __init__(self, interval: int = 8):
        super().__init__(name='PingRepeat')
        self.daemon = True
        self.platform: bool = True
        self.interval: int = interval
        while True:
            try:
                self.destination: str = traceroute('google.com', exit_on_foreign=True)[0]
                break
            except ConnectionError:
                sleep(self.interval)

    def run(self) -> None:
        while not Settings.quitting:
            SinglePing(self.destination).start()
            sleep(self.interval)
