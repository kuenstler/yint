import os
import shlex
import subprocess
from pathlib import Path
from threading import Thread, Lock
from time import sleep

from ZODB.Connection import Connection
from transaction import commit, TransactionManager
from werkzeug.utils import secure_filename

from DB.DownloadList import DownloadListStorage
from DB.Video import VideoStorage, DownloadStatus
from TUI.Runtime import Runtime
from Utilities.Settings import Settings, General
from Utilities.Typings import VID

lock: Lock = Lock()


class DownloadRepeat(Thread):
    def __init__(self):
        """
        A thread to download pending videos
        """
        self._tm: TransactionManager = TransactionManager()
        self._conn: Connection = Settings.db.open(self._tm)
        super().__init__(name='DownloadRepeat')

    def run(self) -> None:
        """
        The actual loop

        Returns
        -------
        None
        """
        while not Settings.quitting:
            self._conn.sync()
            download_list: DownloadListStorage = self._conn.root.download_list
            if download_list and General.downloads_started:
                with lock:
                    self._conn.sync()
                    entry: VID = download_list.get_video()
                    db_entry: VideoStorage = self._conn.root.videos[entry]
                    db_entry.download_status = DownloadStatus.CURRENTLY_DOWNLOADING
                    self._tm.commit()
                while True:
                    try:
                        os.write(Runtime.main_pipe, f'rm_pending\x1f{entry}\n'.encode())
                        break
                    except AttributeError:
                        # init phase
                        sleep(5)
                title: str = db_entry.lurid_title
                os.write(Runtime.main_pipe, f'prog\x1f{title}\x1f0\n'.encode())
                commit()
                video_directory: Path = Settings.general.storage_location / secure_filename(db_entry.title)
                if not video_directory.is_dir():
                    video_directory.mkdir()
                process: subprocess.Popen = subprocess.Popen([
                    *shlex.split(Settings.youtube.youtube_dl_command),
                    f'--{"no-" if db_entry.mark_watched else ""}mark-watched', '--no-warnings', '--newline', '-o',
                    f'{video_directory}/%(title)s-%(id)s.%(ext)s', f'https://youtube.com/watch?v={db_entry.base_id}'
                ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                # initialize a variable to hold the previous percentage value
                previous_percent: str = ''
                # iterate over output lines live
                for line in iter(process.stdout.readline, b''):
                    # decode the line for further operations
                    string: str = line.decode()
                    # if the line is part of the download phase, proceed
                    if string.startswith('[download] '):
                        # this may be something like 36.7% or Destination:
                        potential_percent: str = string.split()[1]
                        # this may be something like 36.7 or Destination
                        current_percent: str = potential_percent[:-1]
                        # if it's percent, use it and check if its another value than the previous one
                        if potential_percent.endswith('%') and current_percent != previous_percent:
                            # send it to the tui while cutting of the percent symbol
                            os.write(Runtime.main_pipe, f'prog\x1f{title}\x1f{current_percent}\n'.encode())
                            # set the previous percentage to the current percentage
                            previous_percent = current_percent
                    # stop downloads if going to stop the application, only works if any lines are inbound, but luckily
                    # youtube-dl's status updates are quite often
                    if Settings.quitting:
                        process.kill()
                return_status: int = process.poll()
                with lock:
                    self._conn.sync()
                    # check for a return status of 0
                    download_list.remove_video(entry)
                    if not return_status:
                        db_entry.download_status = DownloadStatus.ALREADY_DOWNLOADED
                    else:
                        error: str = process.communicate()[1].decode()
                        if error.startswith('ERROR: ') and len(error.splitlines()) == 1:
                            os.write(Runtime.main_pipe, f'prog\x1f{error[:-1]}\x1f100\n'.encode())
                        db_entry.download_status = DownloadStatus.WAITING_FOR_DOWNLOAD_PERMISSION
                        os.write(Runtime.main_pipe, f'add_dwnload\x1f{entry}\n'.encode())
                    self._tm.commit()
            elif Settings.quitting:
                del self
                return
            else:
                sleep(1)
        del self

    def __del__(self):
        """
        Close the database connection while getting removed
        Returns
        -------
        None
        """
        self._conn.close()
