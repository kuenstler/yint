import os
from threading import Thread, Lock
from time import sleep

from ZODB.Connection import Connection

from DB.PingList import PingListStorage
from TUI.Runtime import Runtime
from Utilities.Settings import Settings

lock: Lock = Lock()


class OnlineCheck(Thread):
    def __init__(self):
        self._conn: Connection = Settings.db.open()
        self._no_internet = False
        super().__init__()
        self.daemon = True

    def run(self) -> None:
        while True:
            self._conn.sync()
            pings: PingListStorage = self._conn.root.pings
            score = pings.calculate_score()
            if score == 0:
                os.write(Runtime.main_pipe, 'txt\x1fYour internet is down\n'.encode())
            elif score < 0.7:
                os.write(Runtime.main_pipe, f'txt\x1fYour internet is{" very" if score < 0.5 else ""}'
                                            f' unreliable with a score of {score}\n'.encode())
            elif self._no_internet:
                os.write(Runtime.main_pipe, 'txt\x1fYour internet seems to be normal again\n'.encode())
            self._no_internet = score < 0.7
            sleep(500)
